[![pipeline status](https://gitlab.com/sgdf/api-intranet-java/badges/master/pipeline.svg)](https://gitlab.com/sgdf/api-intranet-java/-/commits/master) [![coverage report](https://gitlab.com/sgdf/api-intranet-java/badges/master/coverage.svg)](https://gitlab.com/sgdf/api-intranet-java/-/commits/master)

# API Intranet

## Présentation

Cette librairie est l'implémentation Java de l'API Intranet SGDF ([Scouts et Guides de France](https://www.sgdf.fr)). Elle prend en charge la négociation du token OAuth2, et tous les appels disponibles.

Elle repose sur :
- Eclipse Jersey 2.x ([lien](https://eclipse-ee4j.github.io/jersey/)) pour la partie client REST
- Apache HttpComponents ([lien](http://hc.apache.org/)) pour le transport
- slf4j ([lien](http://www.slf4j.org/)) pour le logging

## Utilisation

Pour utiliser l'API, il faut :
- Ses identifiants intranet
- un client_id (mis à disposition pour le SI des Scouts et Guides de France)

## Outils
- Java 8 (minimum)
- Eclipse

## Build
- git clone https://gitlab.com/sgdf/api-intranet.git
- cd api-intranet
- mvnw install

## Licence
MIT License ([lien](./LICENSE))
