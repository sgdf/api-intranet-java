package fr.sgdf.intranetapi;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.keycloak.OAuth2Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.sgdf.intranetapi.exceptions.ApiException;

public class ApiUserAuthenticator {
	private final Logger log_ = LoggerFactory.getLogger(ApiUserAuthenticator.class);
	private final WebTarget target_;
	private final String clientId_;
	private final String clientSecret_;

	public ApiUserAuthenticator(String url, String realms, String tokenPath, String clientId, String clientSecret) {
		clientId_ = clientId;
		clientSecret_ = clientSecret;
		Client client = ClientBuilder.newBuilder().build();
		target_ = client.target(UriBuilder.fromUri(url).path(realms).path(tokenPath).build());
	}

	public boolean check(final String username, final String password) throws ApiException {
		Form getTokenForm = new Form() //
				.param("client_id", clientId_).param("client_secret", clientSecret_).param("username", username)
				.param("password", password).param("grant_type", OAuth2Constants.PASSWORD);
		Response response = target_.request(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.form(getTokenForm));
		try {
			if (response.getStatus() == Status.UNAUTHORIZED.getStatusCode()) {
				return false;
			}
			if (response.getStatus() != Status.OK.getStatusCode()) {
				throw new ApiException("Authentication error"); 
			}
			String access_token = response.readEntity(String.class);
			if (log_.isDebugEnabled()) log_.debug("Authentication successful, access_token={}", access_token);
			return true;
		} catch (Exception e) {
			log_.error("Authentication error", e);
			return false;
		}
	};
}
