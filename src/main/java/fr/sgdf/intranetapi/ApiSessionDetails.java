package fr.sgdf.intranetapi;

import java.time.Duration;
import java.time.Instant;

import fr.sgdf.intranetapi.modeles.token.Token;

/**
 * Classe permettant de suivre l'activité d'une session
 * 
 * @author Scouts et Guides de France
 *
 */
public class ApiSessionDetails {
	
	/**
	 * Date de la création de la session
	 */
	public Instant creation;
	
	/**
	 * Date de la génération du jeton 
	 */
	public Instant generationToken;
	
	/**
	 * Date de dernière activité
	 */
	public Instant derniereActivite;
	
	/**
	 * Jeton de connexion renvoyé par le serveur
	 */
	public Token token;
	
	private static int REFRESH_SESSION = 60;
	
	@Override
    public String toString() {
		return token.toString()+", creation="+creation+", generationToken="+generationToken+", derniereActivite="+derniereActivite;
	}
	
	public boolean expired(long maxDelay) {
		if (derniereActivite != null) {
			Instant now = Instant.now();
			long delta = now.getEpochSecond() - derniereActivite.getEpochSecond();
			return (delta > maxDelay);
		}
		return false;
	}
	
	public boolean expired() {
		if (derniereActivite != null) {
			Instant now = Instant.now();
			long delta = now.getEpochSecond() - derniereActivite.getEpochSecond();
			return (delta > token.expires_in - REFRESH_SESSION);
		}
		return false;
	}
	
	Duration getIdleDuration() {
		if (derniereActivite != null) {
			Instant now = Instant.now();
			long delta = now.getEpochSecond() - derniereActivite.getEpochSecond();
			return Duration.ofSeconds(delta);
		}
		return Duration.ofSeconds(60);
	}
	
	public boolean ping() {
		return this.ping(true);
	}
	
	public boolean tokenExpired() {
		return this.expired();
	}
	
	public boolean ping(boolean update) {
		if (update) derniereActivite = Instant.now();
		long deltaRefreshToken = derniereActivite.getEpochSecond() - generationToken.getEpochSecond();
		return (deltaRefreshToken > (token.expires_in - REFRESH_SESSION));
	}
}
