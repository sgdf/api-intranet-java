package fr.sgdf.intranetapi.utils;

public class MetricsConsts {
	public final static String METRICS_API_CALL = "sgdf_intranet_api_call";
	
	public final static String METRICS_API_TAG_GROUP = "api_group";
	public final static String METRICS_API_TAG_CALL = "api_call";
	public final static String METRICS_API_TAG_METHOD = "api_method";
}
