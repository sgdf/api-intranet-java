package fr.sgdf.intranetapi.utils;

import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.modeles.structure.Structure;

public class StructureUtils {
	private static final int STRUCTURE_NATIONALE = 980000000;
	
	public static boolean isNationale(int codeStructure) {
		return codeStructure > STRUCTURE_NATIONALE;
	}
	
	public static boolean isTerritoire(int codeStructure) {
		if (isNationale(codeStructure) == true) {
			return false;
		}
		
		int codeTerritoire = getCodeTerritoire(codeStructure);
		return (codeTerritoire == codeStructure);
	}
	
	public static boolean isGroupe(int codeStructure) {
		if (isNationale(codeStructure) == true) {
			return false;
		}
		if (isTerritoire(codeStructure) == true) {
			return false;
		}
		
		int codeTerritoire = getCodeGroupe(codeStructure);
		return (codeTerritoire == codeStructure);
	}
	
	public static boolean isUnite(int codeStructure) {
		if (isNationale(codeStructure) == true) {
			return false;
		}
		if (isTerritoire(codeStructure) == true) {
			return false;
		}
		if (isGroupe(codeStructure) == true) {
			return false;
		}
		return true;
	}
	
	public static int getCodeTerritoire(int codeStructure) {
		codeStructure/= 10000;
		codeStructure*= 10000;
		return codeStructure; 
	}
	
	public static int getCodeGroupe(int codeStructure) {
		codeStructure/= 100;
		codeStructure*= 100;
		return codeStructure; 
	}
	
	public static Branche getBrancheFromCode(int codeStructure) {
		Branche branche = Branche.ADULTE;
		if (StructureUtils.isUnite(codeStructure)) {
			String brancheSt = getCodebranche(codeStructure);
			if (brancheSt.compareTo("7") == 0)
			{
				return Branche.FARFADET;
			}
			if (brancheSt.compareTo("1") == 0)
			{
				return Branche.LOUVETEAU_JEANNETTE;
			}
			if (brancheSt.compareTo("2") == 0)
			{
				return Branche.SCOUT_GUIDE;
			}
			if (brancheSt.compareTo("3") == 0)
			{
				return Branche.PIONNIER_CARAVELLE;
			}
			if (brancheSt.compareTo("4") == 0)
			{
				return Branche.COMPAGNON;
			}
			if (brancheSt.compareTo("9") == 0)
			{
				return Branche.AUDACE;
			}
			if (brancheSt.compareTo("8") == 0)
			{
				return Branche.IMPEESA;
			}
		}
		return branche;
	}
	
	private static String getCodebranche(int codeStructure)
	{
		String cs = codeStructure+"";
		String codeBranche = cs.substring(cs.length()-2, cs.length()-1);
		return codeBranche;
	}

	static public boolean isTypeNational(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_NATIONAL));
	}

	static public boolean isTypeTerroire(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_TERRITOIRE));
	}

	static public boolean isTypeImpeesaLocal(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_IMPEESA_LOCAL));
	}

	static public boolean isTypeImpeesaTerritorial(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_IMPEESA_TERRITORIAL));
	}

	static public boolean isTypeImpeesaNational(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_IMPEESA_NATIONAL));
	}

	static public boolean isTypeCordeeAudace(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_CORDEE_AUDACE));
	}

	static public boolean isTypeMembresAssocies(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_MEMBRES_ASSOCIES));
	}

	static public boolean isTypeMembresAssociesTerritorial(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_MEMBRES_ASSOCIES_TERRITORIAL));
	}

	static public boolean isTypeAutres(String typeStructure) {
		return (typeStructure.contains(Structure.STRUCTURE_TYPE_AUTRES));
	}

	static public boolean isTypeSommet(String typeStructure) {
		return typeStructure.compareTo(Structure.STRUCTURE_TYPE_SOMMET) == 0;
	}
}
