package fr.sgdf.intranetapi;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.sgdf.intranetapi.exceptions.ApiSessionException;
import fr.sgdf.intranetapi.modeles.Data;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import fr.sgdf.intranetapi.modeles.adherent.AdherentsRequest;
import fr.sgdf.intranetapi.modeles.adherent.DataAdherent;
import fr.sgdf.intranetapi.modeles.adherent.DataAdherents;
import fr.sgdf.intranetapi.modeles.adherent.actionformation.ActionFormation;
import fr.sgdf.intranetapi.modeles.adherent.actionformation.DataActionFormation;
import fr.sgdf.intranetapi.modeles.adherent.diplome.DataDiplomes;
import fr.sgdf.intranetapi.modeles.adherent.diplome.DatasDiplomes;
import fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome;
import fr.sgdf.intranetapi.modeles.adherent.formation.DataFormations;
import fr.sgdf.intranetapi.modeles.adherent.formation.DatasFormations;
import fr.sgdf.intranetapi.modeles.adherent.formation.Formation;
import fr.sgdf.intranetapi.modeles.adherent.historique.AdherentsHistoriqueRequest;
import fr.sgdf.intranetapi.modeles.adherent.historique.DataHistorique;
import fr.sgdf.intranetapi.modeles.adherent.historique.DatasHistorique;
import fr.sgdf.intranetapi.modeles.adherent.historique.Historique;
import fr.sgdf.intranetapi.modeles.adherent.qualification.DataQualifications;
import fr.sgdf.intranetapi.modeles.adherent.qualification.DatasQualifications;
import fr.sgdf.intranetapi.modeles.adherent.qualification.Qualification;
import fr.sgdf.intranetapi.modeles.configuration.Civilite;
import fr.sgdf.intranetapi.modeles.configuration.DataCivilite;
import fr.sgdf.intranetapi.modeles.configuration.DataPays;
import fr.sgdf.intranetapi.modeles.configuration.DataTypeActionFormation;
import fr.sgdf.intranetapi.modeles.configuration.DataTypeDiplome;
import fr.sgdf.intranetapi.modeles.configuration.DataTypeFormation;
import fr.sgdf.intranetapi.modeles.configuration.DataTypeQualification;
import fr.sgdf.intranetapi.modeles.configuration.Pays;
import fr.sgdf.intranetapi.modeles.configuration.TypeActionFormation;
import fr.sgdf.intranetapi.modeles.configuration.TypeDiplome;
import fr.sgdf.intranetapi.modeles.configuration.TypeFormation;
import fr.sgdf.intranetapi.modeles.configuration.TypeQualification;
import fr.sgdf.intranetapi.modeles.formation.DataLieu;
import fr.sgdf.intranetapi.modeles.formation.DataParticipant;
import fr.sgdf.intranetapi.modeles.formation.Lieu;
import fr.sgdf.intranetapi.modeles.formation.Participant;
import fr.sgdf.intranetapi.modeles.formation.Participants;
import fr.sgdf.intranetapi.modeles.parents.DataParents;
import fr.sgdf.intranetapi.modeles.parents.Parent;
import fr.sgdf.intranetapi.modeles.structure.ActionFormationRequest;
import fr.sgdf.intranetapi.modeles.structure.Jeune;
import fr.sgdf.intranetapi.modeles.structure.JeuneData;
import fr.sgdf.intranetapi.modeles.structure.MembreAssocie;
import fr.sgdf.intranetapi.modeles.structure.MembreAssocieData;
import fr.sgdf.intranetapi.modeles.structure.Responsable;
import fr.sgdf.intranetapi.modeles.structure.ResponsableData;
import fr.sgdf.intranetapi.modeles.structure.Structure;
import fr.sgdf.intranetapi.modeles.structure.StructureData;
import fr.sgdf.intranetapi.modeles.structure.StructureRequest;
import fr.sgdf.intranetapi.modeles.token.Token;
import fr.sgdf.intranetapi.utils.MetricsConsts;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

/**
 * Classe représentant une connexion à l'API et par laquelle tous les opérations s'effectuent
 * 
 * @author Scouts et Guides de France
 *
 */
public class ApiCall {
	private final Logger log = LoggerFactory.getLogger(ApiCall.class);

	private final static String urlFormations = "/api/v1/formations";
	private final static String urlAdherents_v1 = "/api/v1/adherents";
	private final static String urlAdherents = "/api/v2/adherents";
	private final static String urlConfiguration = "/api/v1/configuration";
	private final static String urlStructuresRechercher = "/api/v1/structures/rechercher";
	private final static String urlStructures = "/api/v1/structures";
	private final static String urlActionsFormations = "/api/v1/actionsformations";

	private final String clientId_;
	protected final Client client_;
	private boolean broken_ = false;
	protected final String url_;
	private final MeterRegistry registry_;
	
	private final Counter.Builder metricCall = Counter.builder(MetricsConsts.METRICS_API_CALL);
	
	protected final ApiSessionDetails session_ = new ApiSessionDetails();
	
	private final MultivaluedHashMap<String, Object> headers_ = new MultivaluedHashMap<String, Object>();
	
	/**
	 * Constructeur de session (utilisé lors du connexion avec réutilisation d'une session)
	 * @param client HTTP client ({@link Client})
	 * @param clientId Id de connexion (fourni par le SI)
	 * @param url url du serveur (fourni par le SI)
	 */
	public ApiCall(Client client, String clientId, String url, MeterRegistry registry) {
		client_ = client;
		session_.creation = Instant.now();
		clientId_ = clientId;
		url_ = url;
		registry_ = registry;

		session_.token = new Token();
	}
	
	@Override
	public String toString() {
		return "session={"+session_.toString()+"}";
	}
	
	/**
	 * Retourne les informations sur la session
	 * @return les informations sur la session
	 */
	public ApiSessionDetails getSessionInfo() {
		return session_;
	}
	
	/**
	 * Retourne non-activité sur la session
	 * @return la non-activité sur la session
	 */
	public Duration getIdleDuration() {
		return session_.getIdleDuration();
	}

	/**
	 * Retourne l'état de la session
	 * @return état de la session
	 */
	public boolean isBroken() {
		return broken_;
	}
	
	/**
	 * Marque le session comme "cassé"
	 */
	public void setBroken() {
		broken_ = true;
	}

	/**
	 * Vérifie l'état d'une session
	 * @param maxDelay Delai d'expiration
	 * @return l'état d'expiration de la session
	 */
	public boolean expired(long maxDelay) {
		return session_.expired(maxDelay);
	}
	
	/**
	 * Méthode pour mettre à jour le jeton de connexion
	 * @param tokenType type de token
	 * @param token token
	 * @throws ApiSessionException Toute erreur
	 */
	public void updateToken(String tokenType, String token) throws ApiSessionException
	{
		session_.generationToken = Instant.now();
		
		headers_.clear();
		headers_.add("idAppelant", getClientId());
		headers_.add("Authorization", tokenType+" "+token);
	}
	
	private WebTarget generateTarget(String url) {
		if (url_ != null)  {
			return client_.target(url_+"/"+url);
		}
		return client_.target(url);
	}
	
	protected String getClientId() throws ApiSessionException {
		if (clientId_ == null) throw new ApiSessionException("Pas de clientId");
		return clientId_;
	}
	
	private static void handleError(String call, StatusType status, Data data) throws ApiSessionException {
		StringBuilder sb = new StringBuilder("Erreur ");
		sb.append("\"").append(call).append("\" : ");
		sb.append(status.getStatusCode()).append("/").append(status.getReasonPhrase());
		if (data.errors != null) {
			sb.append(" - Erreurs : ").append(data.dumpErrors());
		}
		throw new ApiSessionException(sb.toString());
	}
	
	private static boolean validateResponse(StatusType status, Data data, Object datai) {
		boolean ret = validateResponse(status);
		if ((data.errors != null && data.errors.isEmpty() == false) && datai == null) ret = false;
		return ret;
	}

	private static boolean validateResponse(StatusType status) {
		if (status.getStatusCode() == Status.OK.getStatusCode()) return true;
		if (status.getStatusCode() == Status.CREATED.getStatusCode()) return true;
		return false;
	}
	
	/**
	 * Retourne les informations sur un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return La version JSON d'un adhérent
	 * @throws ApiSessionException Toute erreur
	 */
	public String adherents_brut(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("adherents_brut (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "AdherentV2").tag(MetricsConsts.METRICS_API_TAG_CALL, "adherent").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents);
		try (Response response = target.path(String.valueOf(codeAdherent)).request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataAdherent data = response.readEntity(DataAdherent.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				data.compute();
				if (log.isDebugEnabled()) log.debug("adherents_brut (codeAdherent={}) => {}", codeAdherent, data.data);
				return data.data != null ? ApiParser.encodeAdherent(data.data) : null;
			} else {
				handleError("adherents_brut", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur adherent_brut",e);
		}
	}
	
	/**
	 * Retourne les informations sur un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return l'objet Adherent ({@link Adherent})
	 * @throws ApiSessionException Toute erreur
	 */
	public Adherent adherents(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("adherent (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "AdherentV2").tag(MetricsConsts.METRICS_API_TAG_CALL, "adherent").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents);
		try (Response response = target.path(String.valueOf(codeAdherent)).request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataAdherent data = response.readEntity(DataAdherent.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				data.compute();
				if (log.isDebugEnabled()) log.debug("adherents (codeAdherent={}) => {}", codeAdherent, data.data);
				return data.data;
			} else {
				handleError("adherents", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur adherents",e);
		}
	}
	
	/**
	 * Retourne les informations sur un ensemble d'adhérents
	 * @param codesAdherent Codes adhérent demandés
	 * @return une liste d'objets Adherent ({@link Adherent})
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Adherent> adherents(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("adherents (multi) (nb={}, ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "AdherentV2").tag(MetricsConsts.METRICS_API_TAG_CALL, "adherents").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents);
		try (Response response = target.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DataAdherents data = response.readEntity(DataAdherents.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				data.compute();
				if (log.isDebugEnabled()) log.debug("adherents (multi) => {}", data.data.size());
				return data.data;
			} else {
				handleError("adherents (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur adherents (multi)",e);
		}
	}

	/**
	 * Retourne les actions de formation d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return Les actions de formation
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long,ActionFormation> adherentActionsFormations(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("adherentActionsFormations (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "actionsformations").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("actionsformations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataActionFormation data = response.readEntity(DataActionFormation.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Long, ActionFormation> mdata = new TreeMap<Long, ActionFormation>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("adherentActionsFormations (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("adherentActionsFormations", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur adherentActionsFormations",e);
		}
	}

	/**
	 * Retourne les parent d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return Les parents de formation
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Parent> parents(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("parents (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "parents").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("parents").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataParents data = response.readEntity(DataParents.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("parents (data.data={})", data.data.size());
				return data.data;
			} else {
				handleError("parents", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur parents",e);
		}
	}
	
	/**
	 * Retourne les lieux de formation
	 * @return Les lieux de formation
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long, Lieu> lieuxFormations() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("lieuxFormations");

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Formation").tag(MetricsConsts.METRICS_API_TAG_CALL, "lieux").register(registry_).increment();
		WebTarget target = generateTarget(urlFormations);
		try (Response response = target.path("lieux").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataLieu data = response.readEntity(DataLieu.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Long, Lieu> mdata = new TreeMap<Long, Lieu>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("lieuxFormations (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("lieuxFormations", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur lieuxFormations",e);
		}
	}
	
	/**
	 * Retourne les types de formation
	 * @return Les types de formation
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Integer, TypeFormation> typesFormations() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("typesFormations");
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Configuration").tag(MetricsConsts.METRICS_API_TAG_CALL, "typesFormations").register(registry_).increment();
		WebTarget target = generateTarget(urlConfiguration);
		try (Response response = target.path("typeFormations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataTypeFormation data = response.readEntity(DataTypeFormation.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Integer, TypeFormation> mdata = new TreeMap<Integer, TypeFormation>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("typesFormations (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("typesFormations", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur typesFormations",e);
		}
	}
	
	/**
	 * Retourne les types d'action de formation
	 * @return Les types d'action de formation
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Integer, TypeActionFormation> typesActionsFormations() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("typesActionsFormations");
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Configuration").tag(MetricsConsts.METRICS_API_TAG_CALL, "typesActionsFormations").register(registry_).increment();
		WebTarget target = generateTarget(urlConfiguration);
		try (Response response = target.path("typeActionsFormations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataTypeActionFormation data = response.readEntity(DataTypeActionFormation.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Integer, TypeActionFormation> mdata = new TreeMap<Integer, TypeActionFormation>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("typesActionsFormations (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("typesActionsFormations", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur typesActionsFormations",e);
		}
	}
	
	/**
	 * Retourne les civilités
	 * @return Les civilités
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Integer, Civilite> civilites() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("typesCivilites");
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Configuration").tag(MetricsConsts.METRICS_API_TAG_CALL, "civilites").register(registry_).increment();
		WebTarget target = generateTarget(urlConfiguration);
		try (Response response = target.path("civilites").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataCivilite data = response.readEntity(DataCivilite.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Integer, Civilite> mdata = new TreeMap<Integer, Civilite>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("typesCivilites (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("typesCivilites", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur typesCivilites",e);
		}
		
	}
	
	/**
	 * Retourne les types de qualification
	 * @return Les types de qualification
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Integer, TypeQualification> typesQualifications() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("typesQualifications");
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Configuration").tag(MetricsConsts.METRICS_API_TAG_CALL, "typesQualifications").register(registry_).increment();
		WebTarget target = generateTarget(urlConfiguration);
		try (Response response = target.path("typeQualification").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataTypeQualification data = response.readEntity(DataTypeQualification.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Integer, TypeQualification> mdata = new TreeMap<Integer, TypeQualification>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("typesQualifications (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("typesQualifications", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur typesQualifications",e);
		}
	}
	
	/**
	 * Retourne les types de diplome
	 * @return Les types de diplome
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Integer, TypeDiplome> typesDiplomes() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("typesFormations");
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Configuration").tag(MetricsConsts.METRICS_API_TAG_CALL, "typesDiplomes").register(registry_).increment();
		WebTarget target = generateTarget(urlConfiguration);
		try (Response response = target.path("typeDiplomes").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataTypeDiplome data = response.readEntity(DataTypeDiplome.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Integer, TypeDiplome> mdata = new TreeMap<Integer, TypeDiplome>();
				data.data.forEach(v -> mdata.put(v.id, v));
				if (log.isDebugEnabled()) log.debug("typesDiplomes (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("typeDiplomes", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur typeDiplomes",e);
		}
	}
	
	/**
	 * Retourne les pays
	 * @return Les pays
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Integer, Pays> pays() throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("pays");
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Configuration").tag(MetricsConsts.METRICS_API_TAG_CALL, "pays").register(registry_).increment();
		WebTarget target = generateTarget(urlConfiguration);
		try (Response response = target.path("pays").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataPays data = response.readEntity(DataPays.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Integer, Pays> mdata = new TreeMap<Integer, Pays>();
				data.data.forEach(v -> mdata.put(v.idPays, v));
				if (log.isDebugEnabled()) log.debug("pays (mdata.data={})", mdata.size());
				return mdata;
			} else {
				handleError("pays (mdata.data={})", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur pays",e);
		}
	}
	
	
	/**
	 * Retourne les formations d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @return Les formations
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long,List<Formation>> formations(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("formations (multi) (nb={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "formations").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("formations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasFormations data = response.readEntity(DatasFormations.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("formations (data.data={})", data.data.size());
				return data.toMap();
			} else {
				handleError("formations (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur formations (multi)",e);
		}
	}
	
	/**
	 * Retourne les formations d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @return La version JSON des formations d'un ensemble d'adhérents
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long, String> formations_brut(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("formations (multi) (nb={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "formations").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("formations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasFormations data = response.readEntity(DatasFormations.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("formations_brut (data.data={})", data.data.size());
				return data.toMapString();
			} else {
				handleError("formations (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur formations (multi)",e);
		}
	}
	
	/**
	 * Retourne les formations d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return Les formations
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Formation> formations(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("formations (codeAdherent={})", codeAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "formations").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("formations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataFormations data = response.readEntity(DataFormations.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("formations (data.data={})", data.data.size());
				return data.data;
			} else {
				handleError("formations", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur formations",e);
		}
	}
	
	/**
	 * Retourne les formations d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return La version JSON des formations d'un adhérent
	 * @throws ApiSessionException Toute erreur
	 */
	public String formations_brut(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("formations (codeAdherent={})", codeAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "formations").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("formations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataFormations data = response.readEntity(DataFormations.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("formations_brut (data.data={})", data.data.size());
				return data.data != null ? ApiParser.encodeFormations(data.data) : null;
			} else {
				handleError("formations_brut", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur formations_brut",e);
		}
	}
	
	/**
	 * Retourne les diplomes d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @return Les diplomes
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long,List<Diplome>> diplomes(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("diplomes (multi) (nb={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "diplomes").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("diplomes").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasDiplomes data = response.readEntity(DatasDiplomes.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("diplomes (data.data={})", data.data.size());
				return data.toMap();
			} else {
				handleError("diplomes (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur Diplomes (multi)",e);
		}
	}
	
	/**
	 * Retourne les diplomes d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @return La version JSON des diplomes d'un ensemble d'adhérents
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long, String> diplomes_brut(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("diplomes (multi) (nb={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "diplomes_brut").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("diplomes").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasDiplomes data = response.readEntity(DatasDiplomes.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("diplomes_brut (data.data={})", data.data.size());
				return data.toMapString();
			} else {
				handleError("diplomes (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur diplomes (multi)",e);
		}
	}
	
	/**
	 * Retourne les diplomes d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return La version JSON des diplomes d'un adhérent
	 * @throws ApiSessionException Toute erreur
	 */
	public String diplomes_brut(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("formations (codeAdherent={})", codeAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "diplomes_brut").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("diplomes").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataDiplomes data = response.readEntity(DataDiplomes.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("diplomes_brut (data.data={})", data.data.size());
				return data.data != null ? ApiParser.encodeDiplomes(data.data) : null;
			} else {
				handleError("diplomes_brut", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur diplomes_brut",e);
		}
	}

	/**
	 * Retourne les diplomes d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return Les diplomes
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Diplome> diplomes(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("diplomes (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "diplomes").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("diplomes").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataDiplomes data = response.readEntity(DataDiplomes.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("diplomes (data.data={})", data.data.size());
				return data.data;
			} else {
				handleError("diplomes", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur diplomes",e);
		}
	}
	
	/**
	 * Retourne les qualifications d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @return Les qualifications
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long,List<Qualification>> qualifications(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("qualifications (multi) (nb={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "qualifications").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("qualifications").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasQualifications data = response.readEntity(DatasQualifications.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("qualifications (data.data={})", data.data.size());
				return data.toMap();
			} else {
				handleError("qualifications (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur qualifications (multi)",e);
		}
	}
	
	/**
	 * Retourne les qualifications d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @return La version JSON des qualifications d'un ensemble d'adhérents
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long, String> qualifications_brut(List<Long> codesAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("qualifications (multi) (nb={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsRequest request = new AdherentsRequest(codesAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "qualifications").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("qualifications").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasQualifications data = response.readEntity(DatasQualifications.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("qualifications_brut (data.data={})", data.data.size());
				return data.toMapString();
			} else {
				handleError("qualifications (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur qualifications (multi)",e);
		}
	}
	
	/**
	 * Retourne les qualifications d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return La version JSON des qualifications d'un adhérent
	 * @throws ApiSessionException Toute erreur
	 */
	public String qualifications_brut(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("formations (codeAdherent={})", codeAdherent);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "qualifications").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("qualifications").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataQualifications data = response.readEntity(DataQualifications.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("qualifications_brut (data.data={})", data.data.size());
				return data.data != null ? ApiParser.encodeQualifications(data.data) : null;
			} else {
				handleError("qualifications_brut", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur qualifications_brut",e);
		}
	}
	
	/**
	 * Retourne les qualifications d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @return Les qualifications
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Qualification> qualifications(Long codeAdherent) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("qualifications (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "qualifications").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("qualifications").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataQualifications data = response.readEntity(DataQualifications.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				if (log.isDebugEnabled()) log.debug("qualifications (data.data={})", data.data.size());
				return data.data;
			} else {
				handleError("qualifications", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur qualifications",e);
		}
	}
	
	/**
	 * Retourne l'historique d'un adhérent
	 * @param codeAdherent Code adhérent demandé
	 * @param nombreAnnees Le nombre d'années demandé
	 * @return L'historique
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Historique> historique(Long codeAdherent, int nombreAnnees) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("historique (multi) (codeAdherent={})", codeAdherent);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "historique").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path(String.valueOf(codeAdherent)).path("historique").queryParam("nombreAnnees", nombreAnnees).request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataHistorique data = response.readEntity(DataHistorique.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				return data.data;
			} else {
				handleError("historique (multi)", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur historique (multi)",e);
		}
	}
	
	/**
	 * Retourne l'historique d'un ensemble d'adhérents
	 * @param codesAdherent Code adhérent demandé
	 * @param nombreAnnees Le nombre d'années demandé
	 * @return L'historique
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long,List<Historique>> historiques(List<Long> codesAdherent, int nombreAnnees) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("historiques (codesAdherent={}), ids={})", codesAdherent.size(), codesAdherent.stream().map(n -> n.toString()).collect(Collectors.joining(",")));

		AdherentsHistoriqueRequest request = new AdherentsHistoriqueRequest(codesAdherent, nombreAnnees);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Adherent").tag(MetricsConsts.METRICS_API_TAG_CALL, "historiques").register(registry_).increment();
		WebTarget target = generateTarget(urlAdherents_v1);
		try (Response response = target.path("historiques").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			DatasHistorique data = response.readEntity(DatasHistorique.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				return data.toMap();
			} else {
				handleError("historiques", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur historiques",e);
		}
	}
	
	/**
	 * Retourne les structures
	 * @param request paramètre de la requête
	 * @return la liste des structures
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Structure> structures(StructureRequest request) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("structures (request={})", request.toString());
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Structure").tag(MetricsConsts.METRICS_API_TAG_CALL, "structures").register(registry_).increment();
		WebTarget target = generateTarget(urlStructuresRechercher);
		try (Response response = target.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			StructureData data = response.readEntity(StructureData.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				data.complete();
				if (log.isDebugEnabled()) log.debug("structures (data.data.results={})", data.data.results.size());
				return data.data.results;
			} else {
				handleError("structures", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur structures",e);
		}
	}
	
	/**
	 * Retourne la liste des actions de formation d'une structure
	 * @param codeStructure la structure demandée
	 * @param requestType le type de requête ({@link ActionFormationRequest})
	 * @return la liste des actions de formation
	 * @throws ApiSessionException Toute erreur
	 */
	public Map<Long, fr.sgdf.intranetapi.modeles.structure.ActionFormation> structuresActionsFormations(Integer codeStructure, int requestType) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("structuresActionsFormations (codeAdherent={})", codeStructure);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.POST).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Structure").tag(MetricsConsts.METRICS_API_TAG_CALL, "structuresActionsFormations").register(registry_).increment();
		WebTarget target = generateTarget(urlStructures);
		ActionFormationRequest request = new ActionFormationRequest();
		request.structureDependantes = requestType;
		try (Response response = target.path(String.valueOf(codeStructure)).path("actionsFormations").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).post(Entity.json(request))) {
			fr.sgdf.intranetapi.modeles.structure.DataActionFormation data = response.readEntity(fr.sgdf.intranetapi.modeles.structure.DataActionFormation.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Map<Long, fr.sgdf.intranetapi.modeles.structure.ActionFormation> mdata = new TreeMap<Long, fr.sgdf.intranetapi.modeles.structure.ActionFormation>();
				data.data.forEach(v -> mdata.put(v.id, v));
				return mdata;
			} else {
				handleError("structuresActionsFormations", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur structuresActionsFormations",e);
		}
	}
	
	/**
	 * Retourne les participants d'une action de formation
	 * @param idActionFormation id de la formation
	 * @return la liste des participants
	 * @throws ApiSessionException Toute erreur
	 */
	public Participants actionsFormationsParticipants(Long idActionFormation) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("actionsFormationsParticipants (idActionFormation={})", idActionFormation);

		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "ActionsFormations").tag(MetricsConsts.METRICS_API_TAG_CALL, "actionsFormationsParticipants").register(registry_).increment();
		WebTarget target = generateTarget(urlActionsFormations);
		try (Response response = target.path(String.valueOf(idActionFormation)).path("participants").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			DataParticipant data = response.readEntity(DataParticipant.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				Participants participants = new Participants();
				participants.adherents = new TreeMap<Long, Participant>();
				data.data.forEach(v -> 
				{
					if (v.codeAdherent != null) {
						participants.adherents.put(v.codeAdherent, v);
					} else {
						participants.nbExternes++;
					}
				});
				return participants;
			} else {
				handleError("actionsFormationsParticipants", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur actionsFormationsParticipants",e);
		}
	}
	
	/**
	 * Retourne les responsables d'une structure
	 * @param codeStructure la structure demandée
	 * @return la liste des responsables
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Responsable> responsables(Integer codeStructure) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("responsables (codeStructure={})", codeStructure);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Structure").tag(MetricsConsts.METRICS_API_TAG_CALL, "responsables").register(registry_).increment();
		WebTarget target = generateTarget(urlStructures);
		try (Response response = target.path(String.valueOf(codeStructure)).path("responsables").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			ResponsableData data = response.readEntity(ResponsableData.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				return data.data;
			} else {
				handleError("responsables", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur responsables",e);
		}
	}
	
	/**
	 * Retourne les jeunes d'une structure
	 * @param codeStructure la structure demandée
	 * @return la liste des jeunes
	 * @throws ApiSessionException Toute erreur
	 */
	public List<Jeune> jeunes(Integer codeStructure) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("jeunes (codeStructure={})", codeStructure);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Structure").tag(MetricsConsts.METRICS_API_TAG_CALL, "jeunes").register(registry_).increment();
		WebTarget target = generateTarget(urlStructures);
		try (Response response = target.path(String.valueOf(codeStructure)).path("jeunes").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			JeuneData data = response.readEntity(JeuneData.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				return data.data;
			} else {
				handleError("jeunes", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur jeunes",e);
		}
	}
	
	/**
	 * Retourne les membres associes d'une structure
	 * @param codeStructure la structure demandée
	 * @return la liste des membres associes
	 * @throws ApiSessionException Toute erreur
	 */
	public List<MembreAssocie> membresAssocies(Integer codeStructure) throws ApiSessionException {
		if (log.isDebugEnabled()) log.debug("jeunes (codeStructure={})", codeStructure);
		
		if (registry_ != null) metricCall.tag(MetricsConsts.METRICS_API_TAG_METHOD, HttpMethod.GET).tag(MetricsConsts.METRICS_API_TAG_GROUP, "Structure").tag(MetricsConsts.METRICS_API_TAG_CALL, "membresAssocies").register(registry_).increment();
		WebTarget target = generateTarget(urlStructures);
		try (Response response = target.path(String.valueOf(codeStructure)).path("membresAssocies").request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).headers(headers_).get()) {
			MembreAssocieData data = response.readEntity(MembreAssocieData.class);
			boolean status = validateResponse(response.getStatusInfo(), data, data.data);
			if (status) {
				return data.data;
			} else {
				handleError("membresAssocies", response.getStatusInfo(), data);
				return null;
			}
		}
		catch(Exception e) {
			throw new ApiSessionException("Erreur membresAssocies",e);
		}
	}
}
