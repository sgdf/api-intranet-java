package fr.sgdf.intranetapi;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.AccessTokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.sgdf.intranetapi.exceptions.ApiSessionException;
import fr.sgdf.intranetapi.exceptions.ApiTokenException;
import fr.sgdf.intranetapi.modeles.token.Token;

public class ApiTokenRequester {
	private final Logger logger = LoggerFactory.getLogger(ApiTokenRequester.class);

	private final static String audience = "b27c656c0e534667b66404aace058215";
	
	private final static String pathOAuth2 = "/oauth2/token";
	
	private Keycloak kc_;
	private Client client_;
	private String clientId_;
	private String url_;

	public ApiTokenRequester(Client client, String clientId, String url) {
		client_ = client;
		clientId_ = clientId;
		url_ = url;
	}
	
	public ApiTokenRequester(String url, String realms, String clientId, String clientSecret) {
		kc_ = KeycloakBuilder.builder().serverUrl(url).realm(realms).clientId(clientId).clientSecret(clientSecret).grantType(OAuth2Constants.CLIENT_CREDENTIALS).build();
	}

	public Token getToken(String login, String password) throws ApiSessionException {
		
		Token t = null;
		
		Form form = new Form();
		form.param("grant_type","password");
		form.param("client_id", clientId_);
		form.param("audience", audience);
		form.param("username", login);
		form.param("password", password);
		
		if (logger.isDebugEnabled()) logger.debug("requestToken");

		WebTarget target = generateTarget(client_, pathOAuth2, url_);
		try (Response response = target.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_FORM_URLENCODED).post(Entity.form(form))) {
			if (response.getStatusInfo().getStatusCode() == Status.OK.getStatusCode()) {
				t = response.readEntity(Token.class);
				return t;
			} else {
				TokenError error = response.readEntity(TokenError.class);
				if (response.getStatusInfo().getStatusCode() == Status.BAD_REQUEST.getStatusCode())
					throw new ApiSessionException("Erreur requestToken", response.getStatusInfo(),
							error);
				throw new ApiSessionException("Erreur requestToken", response.getStatusInfo(), error);
			}
		} catch (Exception e) {
			throw new ApiSessionException("Erreur requestToken", e);
		}
	}
	
	public Token refreshToken(Token token) throws ApiSessionException {
		Token t = null;
		
		if (logger.isDebugEnabled()) logger.debug("refreshToken (token={}", token);
		Form form = new Form();
		form.param("grant_type", "refresh_token");
		form.param("client_id", clientId_);
		form.param("audience", audience);
		form.param("refresh_token", token.refresh_token);
		form.param("no_refresh", "false");

		WebTarget target = generateTarget(client_, pathOAuth2, url_);
		try (Response response = target.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_FORM_URLENCODED).post(Entity.form(form))) {
			if (response.getStatusInfo().getStatusCode() == Status.OK.getStatusCode()) {
				t = response.readEntity(Token.class);
				return t;
			} else {
				TokenError error = response.readEntity(TokenError.class);
				if (response.getStatusInfo().getStatusCode() == Status.BAD_REQUEST.getStatusCode())
					throw new ApiSessionException("Erreur refreshToken", response.getStatusInfo(),
							error);
				throw new ApiSessionException("Erreur refreshToken", response.getStatusInfo(), error);
			}
		} catch (Exception e) {
			throw new ApiSessionException("Erreur refreshToken", e);
		}
	}

	
	private WebTarget generateTarget(Client client, String path, String url) {
		return client.target(url+"/"+path);
	}

	public Token getToken(Token oldtoken) throws ApiTokenException {
		if (logger.isDebugEnabled()) logger.debug("Requesting new applicative token");
		if (kc_ == null) {
			try {
				Token token = refreshToken(oldtoken);
				if (logger.isDebugEnabled()) logger.debug("Token: {}", token);
				if (logger.isDebugEnabled()) logger.debug("Token received: {}", token.access_token);
				return token;
			} catch (ApiSessionException ex) {
				throw new ApiTokenException("Error while requesting token", ex);
			}
		}

		Token token = new Token();
		AccessTokenResponse t = null;
		try {
			 t = kc_.tokenManager().getAccessToken();
		}
		catch (Exception ex) {
			throw new ApiTokenException("Error while requesting token", ex);
		}
		
		if (logger.isDebugEnabled()) logger.debug("Token Type: {}", t.getTokenType());
		if (logger.isDebugEnabled()) logger.debug("Token: {}", t.getToken());
		if (logger.isDebugEnabled()) logger.debug("Expire In: {}", t.getExpiresIn());
		
		token.access_token = t.getToken();
		token.token_type = t.getTokenType();
		token.expires_in = t.getExpiresIn();
		if (logger.isDebugEnabled()) logger.debug("Token received: {}", token.access_token);
		return token;
	}
}
