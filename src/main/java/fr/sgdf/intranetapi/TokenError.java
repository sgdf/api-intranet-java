package fr.sgdf.intranetapi;

public class TokenError {
	public String error;
	public String error_description;
	
	@Override
	public String toString() {
		return "[error: "+error+"][error_description: "+error_description+"]";
	}
}
