package fr.sgdf.intranetapi.modeles.token;

/**
 * Classe contenant les élements renvoyés par le serveur d'authentification après une connexion validée.
 * 
 * @author Scouts et Guides de France
 *
 */
public class Token {
	
	/**
	 * Valeur du jeton (à réutiliser pour chaque accès)
	 */
	public String access_token;
	
	/**
	 * Type de jeton
	 */
	public String token_type;
	
	/**
	 * Délai d'expiration (en secondes)
	 */
	public Long expires_in;
	
	/**
	 * Jeton à utiliser pour renouveller le jeton d'accès
	 */
	public String refresh_token;
	
	@Override
    public String toString() {
		return "access_token="+access_token+", token_type="+token_type+", expires_in="+expires_in+", refresh_token="+refresh_token;
	}
}
