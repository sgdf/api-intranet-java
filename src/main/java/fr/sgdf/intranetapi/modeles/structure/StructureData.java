package fr.sgdf.intranetapi.modeles.structure;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class StructureData extends Data {
	public static class StructureResults {
		public List<Structure> results;
		public Long totalResults;
		
		@Override
	    public String toString() {
			return "totalResults="+totalResults+",data=["+results.stream().map(Structure::toString).collect(Collectors.joining(","))+"]";
		}
	}
	
	public StructureResults data;
	
	public void complete() {
		data.results.forEach(structure -> structure.complete());
	}
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data="+data.toString();
	}
}
