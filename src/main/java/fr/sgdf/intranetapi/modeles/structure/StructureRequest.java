package fr.sgdf.intranetapi.modeles.structure;

public class StructureRequest {
	
	public static class Pagination {
		public Long pageSize;
		public Long page;
		
		@Override
	    public String toString() {
			return "pageSize="+(pageSize != null ? pageSize : "null")+", page="+(page != null ? page : "null");
		}
	}
	
	public Long idSaison;
	public Long statutStructure;
	public Pagination pagination;
	
	@Override
    public String toString() {
		return "idSaison="+(idSaison != null?idSaison:"null")+", statutStructure="+(statutStructure != null ? statutStructure : "null")+", pagination=["+(pagination != null ? pagination.toString() : "null")+"]";
	}
}
