package fr.sgdf.intranetapi.modeles.structure;

public class ActionFormationRequest {
	public static final int structureSeule = 0;
	public static final int fillePremierNiveau = 1;
	public static final int fillesEnCascade = 2;
	
	public int structureDependantes;
}
