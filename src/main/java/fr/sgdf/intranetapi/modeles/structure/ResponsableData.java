package fr.sgdf.intranetapi.modeles.structure;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class ResponsableData extends Data {
	public List<Responsable> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Responsable::toString).collect(Collectors.joining(","))+"]";
	}
}
