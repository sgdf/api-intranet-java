package fr.sgdf.intranetapi.modeles.structure;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class MembreAssocieData extends Data {
	public List<MembreAssocie> data;

	@Override
	public String toString() {
		return "errors=" + super.toString() + ",data=["
				+ data.stream().map(MembreAssocie::toString).collect(Collectors.joining(",")) + "]";
	}
}
