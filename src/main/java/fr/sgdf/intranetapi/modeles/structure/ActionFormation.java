package fr.sgdf.intranetapi.modeles.structure;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class ActionFormation {
	
	public Long id;
	public String libelle;
	
	public static enum Status {
		@JsonProperty("0") Preparation,
		@JsonProperty("1") Ouverte,
		@JsonProperty("2") Fermee,
		@JsonProperty("3") Annulee,
		@JsonProperty("4") Cloturee,
		@JsonProperty("5") Invisible,
		@JsonProperty("6") InscriptionsBloquees
	}
	public Status statutFormation;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateDebut;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateFin;
	
	public Integer typeStage;
	public Integer actionFormationTypeId;
	public Integer nbStagiaireMax;
	public Integer tailleLimiteListeAttente;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateNaissanceMin;
	
	public Integer nbStagiaireAcceptes;
	public Integer nbStagiaireDemandes;
	public Integer nbStagiairePreValides;
	public Integer nbStagiaireAAccepter;
	public Integer nbStagiaireListeAttente;
	public Integer nbStagiaireRefuses;
	public Integer nbStagiaireJsMax;
	public Integer nbStagiaireAnnules;
	
	public static class Tarif {
		public String libelle;
		public Double montant;
	}
	
	public Integer typeFormationId;
	public Integer periodeFormationId;
	
	public Integer lieuId;
	public Integer structureOrganisatrice;
	public Integer structureGestionnaire;
	
	public Tarif[] tarif;
	
	@Override
    public String toString() {
		return "id="+id+",libelle="+libelle+",statutFormation="+statutFormation+",dateDebut="+dateDebut.toString()+",dateFin="+dateFin.toString()+
				",typeStage="+typeStage+",actionFormationTypeId="+actionFormationTypeId+",nbStagiaireMax="+nbStagiaireMax+
				",tailleLimiteListeAttente="+tailleLimiteListeAttente+",actionFormationTypeId="+actionFormationTypeId+",tailleLimiteListeAttente="+tailleLimiteListeAttente+
				",dateNaissanceMin="+(dateNaissanceMin != null ? dateNaissanceMin.toString() : "<null>")+",actionFormationTypeId="+nbStagiaireAcceptes+",nbStagiaireDemandes="+nbStagiaireDemandes+
				",nbStagiairePreValides="+nbStagiairePreValides+",nbStagiaireAAccepter="+nbStagiaireAAccepter+",nbStagiaireListeAttente="+nbStagiaireListeAttente+
				",nbStagiaireRefuses="+nbStagiaireRefuses+",tarif="+tarif.length+",typeFormationId="+typeFormationId+",periodeFormationId="+periodeFormationId+
				",lieuId="+lieuId+",structureOrganisatrice="+structureOrganisatrice+",structureGestionnaire="+structureGestionnaire;
	}
}
