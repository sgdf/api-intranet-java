package fr.sgdf.intranetapi.modeles.structure;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataActionFormation extends Data {
	public List<ActionFormation> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(ActionFormation::toString).collect(Collectors.joining(","))+"]";
	}
}
