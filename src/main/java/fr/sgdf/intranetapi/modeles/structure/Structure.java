package fr.sgdf.intranetapi.modeles.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.utils.StructureUtils;

public class Structure {
	
	public static final String STRUCTURE_ECHELON_LOCAL = "Local";
	public static final String STRUCTURE_ECHELON_TERRITORIAL = "Territorial";
	public static final String STRUCTURE_ECHELON_NATIONAL = "National";
	
	public static final String STRUCTURE_TYPE_GROUPE = "Groupe";
	public static final String STRUCTURE_TYPE_TERRITOIRE = "Territoire";
	public static final String STRUCTURE_TYPE_NATIONAL = "Centre National";
	public static final String STRUCTURE_TYPE_SOMMET = "Sommet";
	public static final String STRUCTURE_TYPE_MEMBRES_ASSOCIES = "Membres associés";
	public static final String STRUCTURE_TYPE_MEMBRES_ASSOCIES_TERRITORIAL = "Membres associés Territorial";
	public static final String STRUCTURE_TYPE_AUTRES = "Autres";
	public static final String STRUCTURE_TYPE_IMPEESA_LOCAL = "Réseau Impeesa local";
	public static final String STRUCTURE_TYPE_IMPEESA_NATIONAL = "Réseau Impeesa national";
	public static final String STRUCTURE_TYPE_IMPEESA_TERRITORIAL = "Réseau Impeesa territorial";
	public static final String STRUCTURE_TYPE_CORDEE_AUDACE = "Cordée Audace";
	
	public Long idInvariantStructure;
	public Long idSaison;
	public Integer codeStructure;
	public String nomStructure;
	public String typeStructure;
	public String echelonStructure;
	public Branche branche;
	public String departementAdministratif;
	
	public String description;
	public String hemisphere;
	public String adresseLigne1;
	public String adresseLigne2;
	public String adresseLigne3;
	public String codePostal;
	public String ville;
	public String pays;
	public String telephone;
	public String fax;
	public String courriel;
	public String siteWeb;
	public String debutActivite;
	public String infosLocalisation;
	public String latitude;
	public String longitude;
	
	public Integer getCodeStructure() {
		return codeStructure;
	}

	public void complete() {
		branche = StructureUtils.getBrancheFromCode(this.codeStructure);
	}
	
	@Override
    public String toString() {
		return "idInvariantStructure="+idInvariantStructure+", idSaison="+idSaison+", codeStructure="+codeStructure+", nomStructure="+nomStructure+", typeStructure="+typeStructure+", echelonStructure="+echelonStructure+", branche="+branche+",departementAdministratif="+departementAdministratif
				+",description="+description+",hemisphere="+hemisphere+",adresseLigne1="+adresseLigne1+",adresseLigne2="+adresseLigne2+",adresseLigne3="+adresseLigne3+",codePostal="+codePostal+",ville="+ville+",pays="+pays+",telephone="+telephone
				+",fax="+fax+",courriel="+courriel+",debutActivite="+debutActivite+",infosLocalisation="+infosLocalisation+",latitude="+latitude+",longitude="+longitude;
	}
	
	@JsonIgnore
	public boolean isTypeGroupe() {
		return this.isEchelonLocale() && (typeStructure.compareTo(STRUCTURE_TYPE_GROUPE) == 0);
	}

	@JsonIgnore
	public boolean isTypeUnite() {
		return this.isEchelonLocale() && (typeStructure.compareTo(STRUCTURE_TYPE_GROUPE) != 0);
	}

	@JsonIgnore
	public boolean isTypeTerritoire() {
		return this.isEchelonTerritorial() && (typeStructure.compareTo(STRUCTURE_TYPE_TERRITOIRE) == 0);
	}

	@JsonIgnore
	public boolean isTypeImpeesaLocal() {
		return StructureUtils.isTypeImpeesaLocal(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeImpeesaTerritorial() {
		return StructureUtils.isTypeImpeesaTerritorial(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeImpeesaNational() {
		return StructureUtils.isTypeImpeesaNational(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeCordeeAudace() {
		return StructureUtils.isTypeCordeeAudace(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeMembresAssocies() {
		return StructureUtils.isTypeMembresAssocies(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeMembresAssociesTerritorial() {
		return StructureUtils.isTypeMembresAssociesTerritorial(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeAutres() {
		return StructureUtils.isTypeAutres(typeStructure);
	}

	@JsonIgnore
	public boolean isTypeNational() {
		return this.isEchelonNational() && (typeStructure.compareTo(STRUCTURE_TYPE_NATIONAL) == 0);
	}

	@JsonIgnore
	public boolean isTypeSommet() {
		return StructureUtils.isTypeSommet(typeStructure);
	}

	@JsonIgnore
	public boolean isEchelonLocale() {
		return echelonStructure.compareTo(STRUCTURE_ECHELON_LOCAL) == 0;
	}

	@JsonIgnore
	public boolean isEchelonTerritorial() {
		return echelonStructure.compareTo(STRUCTURE_ECHELON_TERRITORIAL) == 0;
	}

	@JsonIgnore
	public boolean isEchelonNational() {
		return echelonStructure.compareTo(STRUCTURE_ECHELON_NATIONAL) == 0;
	}
}
