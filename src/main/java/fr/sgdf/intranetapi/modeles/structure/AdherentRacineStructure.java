package fr.sgdf.intranetapi.modeles.structure;

public class AdherentRacineStructure {
	public Long codeAdherent;
	public Long idCivilite;
	public String nom;
	public String prenom;
	public Integer statut;
	
	public Long getCodeAdherent() {
		return codeAdherent;
	}
	
	@Override
    public String toString() {
		return "codeAdherent="+codeAdherent+", idCivilite="+idCivilite+", nom="+nom+", prenom="+prenom+", statut="+statut;
	}

}
