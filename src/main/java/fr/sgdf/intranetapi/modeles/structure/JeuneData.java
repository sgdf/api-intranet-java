package fr.sgdf.intranetapi.modeles.structure;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class JeuneData extends Data {
	public List<Jeune> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Jeune::toString).collect(Collectors.joining(","))+"]";
	}
}
