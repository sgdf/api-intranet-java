package fr.sgdf.intranetapi.modeles.adherent.diplome;

import java.util.List;
import java.util.stream.Collectors;

public class AdherentDiplome {
	public String nom;
	public String prenom;
	public Long codeAdherent;
	public List<Diplome> diplomes;
	
	@Override
    public String toString() {
		return "nom="+nom+",prenom="+prenom+",codeAdherent="+codeAdherent+",formations=["+diplomes.stream().map(Diplome::toString).collect(Collectors.joining(","))+"]";
	}
}
