package fr.sgdf.intranetapi.modeles.adherent;

import fr.sgdf.intranetapi.modeles.Data;

public class DataAdherent extends Data {
	public Adherent data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data="+data.toString();
	}

	public void compute() {
		data.compute();
	}
}
