package fr.sgdf.intranetapi.modeles.adherent;

public enum Statut {
	ADHERENT,
	PREINSCRIT,
	INVITE,
	A_QUITTE_L_ASSOCATION
}
