package fr.sgdf.intranetapi.modeles.adherent.historique;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DatasHistorique extends Data {
	
	public List<AdherentHistorique> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(AdherentHistorique::toString).collect(Collectors.joining(","))+"]";
	}
	
	public Map<Long,List<Historique>> toMap() {
		Map<Long,List<Historique>> map = new TreeMap<Long, List<Historique>>();
		data.forEach(adf -> {
			map.put(adf.codeAdherent, adf.historique);
		});
		return map;
	}
}
