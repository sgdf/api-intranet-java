package fr.sgdf.intranetapi.modeles.adherent.qualification;

import java.util.List;
import java.util.stream.Collectors;

public class AdherentQualification {
	public String nom;
	public String prenom;
	public Long codeAdherent;
	public List<Qualification> qualifications;
	
	@Override
    public String toString() {
		return "nom="+nom+",prenom="+prenom+",codeAdherent="+codeAdherent+",formations=["+qualifications.stream().map(Qualification::toString).collect(Collectors.joining(","))+"]";
	}
}
