package fr.sgdf.intranetapi.modeles.adherent.formation;

import java.util.List;
import java.util.stream.Collectors;

public class AdherentFormation {
	public String nom;
	public String prenom;
	public Long codeAdherent;
	public List<Formation> formations;
	
	@Override
    public String toString() {
		return "nom="+nom+",prenom="+prenom+",codeAdherent="+codeAdherent+",formations=["+formations.stream().map(Formation::toString).collect(Collectors.joining(","))+"]";
	}
}
