package fr.sgdf.intranetapi.modeles.adherent.formation;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.ApiParser;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.Data;

public class DatasFormations extends Data {
	
	public List<AdherentFormation> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(AdherentFormation::toString).collect(Collectors.joining(","))+"]";
	}
	
	public Map<Long,List<Formation>> toMap() {
		Map<Long,List<Formation>> map = new TreeMap<Long, List<Formation>>();
		data.forEach(adf -> {
			map.put(adf.codeAdherent, adf.formations);
		});
		return map;
	}
	
	public Map<Long,String> toMapString() {
		Map<Long,String> map = new TreeMap<Long, String>();
		data.forEach(adf -> {
			try {
				map.put(adf.codeAdherent, ApiParser.encodeFormations(adf.formations));
			} catch (NumberFormatException | ApiException e) {
			}
		});
		return map;
	}
}
