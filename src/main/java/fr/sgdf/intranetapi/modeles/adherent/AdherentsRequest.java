package fr.sgdf.intranetapi.modeles.adherent;

import java.util.List;

public class AdherentsRequest {
	public AdherentsRequest(List<Long> codesAdherent) {
		this.codesAdherent = codesAdherent.toArray(new Long[0]);
	}

	public Long codesAdherent[];
}
