package fr.sgdf.intranetapi.modeles.adherent.formation;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataFormations extends Data {
	public List<Formation> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Formation::toString).collect(Collectors.joining(","))+"]";
	}
}
