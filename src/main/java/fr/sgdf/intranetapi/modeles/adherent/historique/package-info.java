/**
 * Package qui contient les classes de modèle de l'historique d'un adhérent 
 */
package fr.sgdf.intranetapi.modeles.adherent.historique;
