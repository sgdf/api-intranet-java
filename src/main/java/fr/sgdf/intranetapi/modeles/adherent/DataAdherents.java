package fr.sgdf.intranetapi.modeles.adherent;

import java.util.List;

import fr.sgdf.intranetapi.modeles.Data;

public class DataAdherents extends Data {
	public List<Adherent> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data="+data.size();
	}

	public void compute() {
		data.forEach(data -> data.compute());
	}
}
