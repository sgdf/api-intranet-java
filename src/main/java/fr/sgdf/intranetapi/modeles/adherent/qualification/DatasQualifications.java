package fr.sgdf.intranetapi.modeles.adherent.qualification;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.ApiParser;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.Data;

public class DatasQualifications extends Data {
	
	public List<AdherentQualification> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(AdherentQualification::toString).collect(Collectors.joining(","))+"]";
	}
	
	public Map<Long,List<Qualification>> toMap() {
		Map<Long,List<Qualification>> map = new TreeMap<Long, List<Qualification>>();
		data.forEach(adf -> {
			map.put(adf.codeAdherent, adf.qualifications);
		});
		return map;
	}
	
	public Map<Long,String> toMapString() {
		Map<Long,String> map = new TreeMap<Long, String>();
		data.forEach(adf -> {
			try {
				map.put(adf.codeAdherent, ApiParser.encodeQualifications(adf.qualifications));
			} catch (NumberFormatException | ApiException e) {
			}
		});
		return map;
	}
}
