package fr.sgdf.intranetapi.modeles.adherent.formation;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class Formation {
	public Long id;
	public String libelle;
	public String type;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateFin;
	
	public Long getId() {
		return id;
	}
	
	public static enum Role {
		@JsonProperty("Stagiaire") Stagiaire,
		@JsonProperty("Formateur") Formateur,
		@JsonProperty("Directeur") Directeur,
		@JsonProperty("Accompagnant") Accompagnant
	}
	public Role role;
	
	@Override
    public String toString() {
		return "id="+id+",libelle="+libelle+",type="+type+",dateFin="+dateFin.toString()+",role="+role;
	}
}
