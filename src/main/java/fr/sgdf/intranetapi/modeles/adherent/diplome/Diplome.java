package fr.sgdf.intranetapi.modeles.adherent.diplome;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class Diplome {
	public Long id;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateObtention;
	
	public String type;
	
	public Long getId() {
		return id;
	}
	
	@Override
    public String toString() {
		return "id="+id+",type="+type+",dateFin="+dateObtention.toString();
	}
}
