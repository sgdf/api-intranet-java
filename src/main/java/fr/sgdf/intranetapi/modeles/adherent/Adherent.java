package fr.sgdf.intranetapi.modeles.adherent;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import fr.sgdf.intranetapi.modeles.AdherentRacine;

public class Adherent extends AdherentRacine {

	static final private int CODE_LJ = 110;
	static final private int CODE_SG = 120;
	static final private int CODE_PIOK = 130;
	static final private int CODE_FARFADETS = 170;
	static final private int CODE_COMPAS_T1T2 = 140;
	static final private int CODE_COMPAS_T3 = 141;
	static final private int CODE_AUDACE = 190;

	static final private String STATUS_PREINSCRIT = "Pré-inscrit(e)";
	static final private String STATUS_ADHERENT = "Adhérent(e)";
	static final private String STATUS_INVITE = "Invité(e)";
	static final private String STATUS_A_QUITTE_L_ASSOCATION = "A quitté l'association";

	public static class Structure {
		public Long idInvariantStructure;
		public Integer code;
		public String nom;
		public String echelon;
		public Long id;

		@Override
		public String toString() {
			return "id=" + id + ", idInvariantStructure=" + idInvariantStructure + ", code=" + code + ", nom=" + nom + ", echelon="
					+ echelon;
		}
	};

	public static class Fonction {
		public String code;
		public String nom;
		public Long categorie;
		public Long id;

		@Override
		public String toString() {
			return "code=" + code + ", nom=" + nom + ", categorie=" + categorie + ", id="+id;
		}
	};

	public static class FonctionPrincipale {
		public Structure structure;
		public Fonction fonction;
		public Long inscriptionId;

		@Override
		public String toString() {
			return "structure=" + structure.toString() + ", fonction=" + fonction.toString()+", inscriptionId = "+inscriptionId;
		}
		
		public String toStringWithCurlyBrackets() {
			return "{" + this.toString() + "}";
		}
	}

	public static class LieuDeNaissance {
		public String codePostal;
		public String ville;
		public Long codeInsee;
		public Long idPays;

		@Override
		public String toString() {
			return "codePostal=" + codePostal + ", ville=" + ville + ", codeInsee=" + codeInsee+", idPays="+idPays;
		}
	}

	public static class IntervenantJS {
		public String fonctionJS;
		public String diplomeJS;
		public String diplomeDetailJS;
		public String qualiteJS;

		@JsonSerialize(using = LocalDateTimeSerializer.class)
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		public LocalDateTime derniereModification;

		@Override
		public String toString() {
			return "fonctionJS=" + fonctionJS + ", diplomeJS=" + diplomeJS + ", diplomeDetailJS=" + diplomeDetailJS
					+ ", qualiteJS=" + qualiteJS + ", derniereModification=" + derniereModification.toString();
		}
	}

	public Long idCivilite;
	public String saison;
	public String nomNaissance;
	public Date dateDeNaissance;
	public String statut;
	public Statut statutE;

	public FonctionPrincipale fonctionPrincipale;
	public List<FonctionPrincipale> fonctionsSecondaires;

	public LieuDeNaissance lieuDeNaissance;
	public Branche branche;

	public Boolean droitImage;
	public Boolean droitsInformations;
	
	public Boolean mesuresMedicoChirurgicales;
	public Boolean outilsNumeriques;
	public Boolean assuranceResponsabiliteCivile;
	public Boolean accepteMailsMouvement;
	public Boolean accepteMailsExterieurMouvement;
	public Boolean abonnementNewsletter;

	public IntervenantJS intervenantJS;

	private int extraitCodeFonction(String code) {
		char lcode = code.charAt(code.length() - 1);
		if (lcode > 'A')
			code = code.substring(0, 3);
		return Integer.valueOf(code);
	}

	public void compute() {
		// Detection du status
		if (statut.equals(STATUS_ADHERENT)) {
			statutE = Statut.ADHERENT;
		} else if (statut.equals(STATUS_PREINSCRIT)) {
			statutE = Statut.PREINSCRIT;
		} else if (statut.equals(STATUS_INVITE)) {
			statutE = Statut.INVITE;
		} else if (statut.equals(STATUS_A_QUITTE_L_ASSOCATION)) {
			statutE = Statut.A_QUITTE_L_ASSOCATION;
		} else {
			statutE = Statut.ADHERENT;
		}

		// Detection des branches
		if (fonctionPrincipale != null) {
			Fonction fonction = fonctionPrincipale.fonction;
			int code = extraitCodeFonction(fonction.code);
			switch (code) {
			case CODE_FARFADETS:
				branche = Branche.FARFADET;
				break;
			case CODE_LJ:
				branche = Branche.LOUVETEAU_JEANNETTE;
				break;
			case CODE_SG:
				branche = Branche.SCOUT_GUIDE;
				break;
			case CODE_PIOK:
				branche = Branche.PIONNIER_CARAVELLE;
				break;
			case CODE_COMPAS_T1T2:
			case CODE_COMPAS_T3:
				branche = Branche.COMPAGNON;
				break;
			case CODE_AUDACE:
				branche = Branche.AUDACE;
				break;
			default:
				branche = Branche.ADULTE;
				break;
			}
		} else {
			branche = Branche.INCONNU;
		}
	}

	@Override
	public String toString() {
		return super.toString() + ", nomNaissance=" + nomNaissance + ", statut=" + statut + ", statutE="
				+ statutE.name() + ", fonctionPrincipale=" + fonctionPrincipale.toString()+", fonctionsSecondaires=["+fonctionsSecondaires.stream().map(FonctionPrincipale::toStringWithCurlyBrackets).collect(Collectors.joining(","))+"]"
				+ ", branche=" + branche + ", droitsInformations=" + droitsInformations + ", droitImage=" + droitImage
				+ ", idCivilite=" + idCivilite + ", intervenantJS=["
				+ (intervenantJS != null ? intervenantJS.toString() : "null") + "]";
	}
}
