package fr.sgdf.intranetapi.modeles.adherent;

public enum Branche {
	INCONNU,
	FARFADET,
	LOUVETEAU_JEANNETTE,
	SCOUT_GUIDE,
	PIONNIER_CARAVELLE,
	COMPAGNON,
	AUDACE,
	ADULTE,
	IMPEESA
}
