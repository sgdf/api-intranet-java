package fr.sgdf.intranetapi.modeles.adherent.historique;

import java.util.List;

public class AdherentsHistoriqueRequest {
	public AdherentsHistoriqueRequest(List<Long> codesAdherent, int nombreAnnees) {
		this.codesAdherent = codesAdherent.toArray(new Long[0]);
	}

	public Long codesAdherent[];
	public int nombreAnnees;
}
