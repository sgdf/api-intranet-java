package fr.sgdf.intranetapi.modeles.adherent.diplome;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.ApiParser;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.Data;

public class DatasDiplomes extends Data {
	
	public List<AdherentDiplome> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(AdherentDiplome::toString).collect(Collectors.joining(","))+"]";
	}
	
	public Map<Long,List<Diplome>> toMap() {
		Map<Long,List<Diplome>> map = new TreeMap<Long, List<Diplome>>();
		data.forEach(adf -> {
			map.put(adf.codeAdherent, adf.diplomes);
		});
		return map;
	}
	
	public Map<Long,String> toMapString() {
		Map<Long,String> map = new TreeMap<Long, String>();
		data.forEach(adf -> {
			try {
				map.put(adf.codeAdherent, ApiParser.encodeDiplomes(adf.diplomes));
			} catch (NumberFormatException | ApiException e) {
			}
		});
		return map;
	}
}
