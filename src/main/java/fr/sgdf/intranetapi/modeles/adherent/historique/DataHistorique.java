package fr.sgdf.intranetapi.modeles.adherent.historique;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataHistorique extends Data {
	public List<Historique> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Historique::toString).collect(Collectors.joining(","))+"]";
	}
}
