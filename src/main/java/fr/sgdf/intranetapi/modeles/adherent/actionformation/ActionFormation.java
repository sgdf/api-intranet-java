package fr.sgdf.intranetapi.modeles.adherent.actionformation;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class ActionFormation {
	public Long id;
	public Integer actionFormationTypeId;
	public Integer typeFormationId;
	public String libelle;
	public List<String> libelleParcours;
	public String commentairePrevalidation;
	public String commentaireValidation;
	public String commentaireValidationGestionnaire;
	public String allergiesRegimeAlimentaire;
	public String numeroInscriptionBafaBafd;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateDebut;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateFin;
	
	public Long actionFormationLieuId;
	
	public Long getId() {
		return id;
	}
	
	public static enum StatusInscription {
		@JsonProperty("0") Demandee,
		@JsonProperty("1") PreValidee,
		@JsonProperty("2") Validee,
		@JsonProperty("3") ListeAttente,
		@JsonProperty("4") Acceptee,
		@JsonProperty("5") Refusee,
		@JsonProperty("6") Annulee
	}
	public StatusInscription statutInscription;

	public static enum Role {
		@JsonProperty("0") Stagiaire,
		@JsonProperty("1") Formateur,
		@JsonProperty("2") Directeur,
		@JsonProperty("3") Accompagnant
	}
	public Role roleInscritActionFormation;
	public Boolean formationSatisfaisante;
	
	@Override
    public String toString() {
		return "id="+id+",actionFormationTypeId="+actionFormationTypeId+",typeFormationId="+typeFormationId+",libelle="+libelle+",dateDebut="+dateDebut.toString()+",dateFin="+dateFin.toString()+
				",actionFormationLieuId="+actionFormationLieuId+",roleInscritActionFormation="+roleInscritActionFormation.name()+",formationSatisfaisante="+formationSatisfaisante;
	}
}
