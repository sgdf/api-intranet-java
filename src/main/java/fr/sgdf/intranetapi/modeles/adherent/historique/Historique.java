package fr.sgdf.intranetapi.modeles.adherent.historique;

public class Historique {
	public static class Fonction {
		public String code;
		public String nom;
		
		@Override
	    public String toString() {
			return "code="+code+", nom="+nom;
		}
	};
	
	public static class Structure {
		public Integer code;
		public String nom;
		public String echelon;
		
		@Override
	    public String toString() {
			return "code="+code+", nom="+nom+", echelon="+echelon;
		}
	};
	
	public Long saisonId;
	public Fonction fonction;
	public Structure structure;
	public String categorieMembre;
	
	@Override
    public String toString() {
		return "saisonId"+saisonId+",fonction="+fonction+",structure="+structure+",categorieMembre="+categorieMembre;
	}
}
