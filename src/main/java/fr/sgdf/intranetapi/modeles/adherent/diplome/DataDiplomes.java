package fr.sgdf.intranetapi.modeles.adherent.diplome;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataDiplomes extends Data {
	public List<Diplome> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Diplome::toString).collect(Collectors.joining(","))+"]";
	}
}
