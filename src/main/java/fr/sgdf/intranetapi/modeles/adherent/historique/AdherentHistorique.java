package fr.sgdf.intranetapi.modeles.adherent.historique;

import java.util.List;
import java.util.stream.Collectors;

public class AdherentHistorique {
	public String nom;
	public String prenom;
	public Long codeAdherent;
	public List<Historique> historique;
	
	@Override
    public String toString() {
		return "nom="+nom+",prenom="+prenom+",codeAdherent="+codeAdherent+",formations=["+historique.stream().map(Historique::toString).collect(Collectors.joining(","))+"]";
	}
}
