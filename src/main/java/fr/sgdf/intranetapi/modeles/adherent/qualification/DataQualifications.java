package fr.sgdf.intranetapi.modeles.adherent.qualification;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataQualifications extends Data {
	public List<Qualification> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Qualification::toString).collect(Collectors.joining(","))+"]";
	}
}
