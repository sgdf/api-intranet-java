package fr.sgdf.intranetapi.modeles.adherent.qualification;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class Qualification {
	public Long id;
	public boolean estTitulaire;
	public String type;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime dateFin;
	
	public Long getId() {
		return id;
	}
	
	@Override
    public String toString() {
		return "id="+id+",estTitulaire="+estTitulaire+",dateFin="+(dateFin != null ? dateFin.toString() : "null")+",type="+type;
	}
}
