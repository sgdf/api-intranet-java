package fr.sgdf.intranetapi.modeles.configuration;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataTypeActionFormation extends Data {
	public List<TypeActionFormation> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(TypeActionFormation::toString).collect(Collectors.joining(","))+"]";
	}
}