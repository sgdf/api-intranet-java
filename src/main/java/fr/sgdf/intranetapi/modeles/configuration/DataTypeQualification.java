package fr.sgdf.intranetapi.modeles.configuration;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataTypeQualification extends Data {
	public List<TypeQualification> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(TypeQualification::toString).collect(Collectors.joining(","))+"]";
	}
}