package fr.sgdf.intranetapi.modeles.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeQualification extends Type {
	public static enum Type {
		@JsonProperty("0") Direction,
		@JsonProperty("1") Animation,
		@JsonProperty("2") Formation,
		@JsonProperty("3") Autre
	}
	// Direction = 0 Animation = 1 Formation = 2 Autre = 3 
	public Type typeQualification;
	
	@Override
    public String toString() {
		return super.toString()+",typeQualification="+typeQualification;
	}
}
