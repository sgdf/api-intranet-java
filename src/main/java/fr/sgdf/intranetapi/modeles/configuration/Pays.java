package fr.sgdf.intranetapi.modeles.configuration;

public class Pays {
	public Integer idPays;
	public String libellePays;
	
	public Integer getIdPays() {
		return idPays;
	}
	
	public String getLibellePays() {
		return libellePays;
	}
	
	@Override
    public String toString() {
		return "idPays="+idPays+",libellePays="+libellePays;
	}
}
