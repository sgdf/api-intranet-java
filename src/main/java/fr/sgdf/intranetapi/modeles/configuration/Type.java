package fr.sgdf.intranetapi.modeles.configuration;

public class Type {
	public Integer id;
	public String libelle;
	
	public Integer getId() {
		return id;
	}
	
	@Override
    public String toString() {
		return "id="+id+",libelle="+libelle;
	}
}
