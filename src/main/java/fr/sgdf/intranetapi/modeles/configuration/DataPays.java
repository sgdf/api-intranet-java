package fr.sgdf.intranetapi.modeles.configuration;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataPays extends Data {
	public List<Pays> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Pays::toString).collect(Collectors.joining(","))+"]";
	}
}