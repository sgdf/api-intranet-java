/**
 * Package qui contient les classes de modèle de configuration (types de formation, types de qualification, type de diplôme...) 
 */
package fr.sgdf.intranetapi.modeles.configuration;
