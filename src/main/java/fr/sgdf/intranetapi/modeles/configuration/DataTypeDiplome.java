package fr.sgdf.intranetapi.modeles.configuration;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataTypeDiplome extends Data {
	public List<TypeDiplome> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(TypeDiplome::toString).collect(Collectors.joining(","))+"]";
	}
}