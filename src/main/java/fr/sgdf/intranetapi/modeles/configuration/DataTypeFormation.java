package fr.sgdf.intranetapi.modeles.configuration;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataTypeFormation extends Data {
	public List<TypeFormation> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(TypeFormation::toString).collect(Collectors.joining(","))+"]";
	}
}