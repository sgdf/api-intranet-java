package fr.sgdf.intranetapi.modeles.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Civilite extends Type {
	public static enum Genre {
		@JsonProperty("0") Homme,
		@JsonProperty("1") Femme
	}
	// H=0, F=1 
	public Genre genre;
	
	@Override
    public String toString() {
		return super.toString()+", genre="+genre;
	}
}
