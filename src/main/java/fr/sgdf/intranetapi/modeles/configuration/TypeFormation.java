package fr.sgdf.intranetapi.modeles.configuration;

public class TypeFormation extends Type {
	public Boolean estDisponibleActionTerritoriale;
	
	@Override
    public String toString() {
		return super.toString()+",libelle="+libelle+",estDisponibleActionTerritoriale="+estDisponibleActionTerritoriale;
	}
}
