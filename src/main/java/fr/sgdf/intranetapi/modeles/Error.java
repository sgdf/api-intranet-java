package fr.sgdf.intranetapi.modeles;

import java.util.Date;

public class Error {
	public String message;
	public String details;
	public String errorReference;
    public Date  dateTime;
    
    @Override
    public String toString() {
    	return "message="+message+", details="+details+", errorReference="+errorReference+", dateTime="+dateTime.toString();
    }
}
