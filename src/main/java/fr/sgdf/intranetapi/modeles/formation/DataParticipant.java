package fr.sgdf.intranetapi.modeles.formation;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataParticipant extends Data {
	public List<Participant> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Participant::toString).collect(Collectors.joining(","))+"]";
	}
}