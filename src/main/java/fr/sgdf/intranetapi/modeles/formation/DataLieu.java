package fr.sgdf.intranetapi.modeles.formation;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataLieu extends Data {
	public List<Lieu> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Lieu::toString).collect(Collectors.joining(","))+"]";
	}
}