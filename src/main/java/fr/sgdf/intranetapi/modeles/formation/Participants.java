package fr.sgdf.intranetapi.modeles.formation;

import java.util.Map;
import java.util.stream.Collectors;

public class Participants {
	public Map<Long, Participant> adherents;
	public int nbExternes;
	
	@Override
    public String toString() {
		return "nbExternes="+nbExternes+",adherents=["+adherents.values().stream().map(Participant::toString).collect(Collectors.joining(","))+"]";
	}
}
