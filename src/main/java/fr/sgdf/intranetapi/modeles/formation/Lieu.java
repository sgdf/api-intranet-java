package fr.sgdf.intranetapi.modeles.formation;

public class Lieu {
	
	static public class Adresse {
		public String ligne1;
		public String ligne2;
		public String ligne3;
		public String codePostal;
		public String lieuDit;
		public String ville;
		public String pays;
		public Boolean npai;
		
		@Override
	    public String toString() {
			return "ligne1="+ligne1+", ligne2="+ligne2+", ligne3="+ligne3+", lieuDit="+lieuDit+", codePostal="+codePostal+", ville="+ville+", pays="+pays+", npai="+npai;
		}
	}
	
	public Long id;
	public String libelle;
	public Adresse adresse;
	
	@Override
    public String toString() {
		return adresse.toString()+", libelle="+libelle+", id="+id;
	}
}
