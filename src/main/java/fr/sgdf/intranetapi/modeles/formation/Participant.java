package fr.sgdf.intranetapi.modeles.formation;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Participant {
	public Long codeAdherent;
	
	public static enum Role {
		@JsonProperty("0") Stagiaire,
		@JsonProperty("1") Formateur,
		@JsonProperty("2") Directeur,
		@JsonProperty("3") Accompagnant
	}
	public Role roleInscritActionFormation;
	
	public List<String> libelleParcours;
	public String commentairePrevalidation;
	public String commentaireValidation;
	public String commentaireValidationGestionnaire;
	public String statutInscription;
	public String allergiesRegimeAlimentaire;
	public String numeroInscriptionBafaBafd;
	
	public Long getCodeAdherent() {
		return codeAdherent;
	}
	
	@Override
    public String toString() {
		return "codeAdherent="+codeAdherent+",roleInscritActionFormation="+roleInscritActionFormation.name()+",commentairePrevalidation="+commentairePrevalidation+",commentaireValidation="+commentaireValidation+",commentaireValidationGestionnaire="+commentaireValidationGestionnaire+
",statutInscription"+statutInscription+",allergiesRegimeAlimentaire="+allergiesRegimeAlimentaire+",numeroInscriptionBafaBafd="+numeroInscriptionBafaBafd+",libelleParcours="+libelleParcours.stream().collect(Collectors.joining(","));
	}
}
