package fr.sgdf.intranetapi.modeles.parents;

import fr.sgdf.intranetapi.modeles.AdherentRacine;

public class Parent extends AdherentRacine {
	
	public String typeParent;
	
	@Override
    public String toString() {
		return super.toString()+",typeParent="+typeParent;
	}
}
