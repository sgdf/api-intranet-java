package fr.sgdf.intranetapi.modeles.parents;

import java.util.List;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.Data;

public class DataParents extends Data {
	public List<Parent> data;
	
	@Override
    public String toString() {
		return "errors="+super.toString()+",data=["+data.stream().map(Parent::toString).collect(Collectors.joining(","))+"]";
	}
}
