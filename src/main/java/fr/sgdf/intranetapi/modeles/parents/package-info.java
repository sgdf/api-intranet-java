/**
 * Package qui contient les classes de modèle liés aux parents des adhérents
 */
package fr.sgdf.intranetapi.modeles.parents;
