package fr.sgdf.intranetapi.modeles;

import java.util.List;
import java.util.stream.Collectors;

public class AdherentRacine {
	public Long codeAdherent;
	public String nom;
	public String nomNaissance;
	public String prenom;
	public String prenomCivil;
	public List<String> emails;
	
	public String telephoneBureau;
	public String telephoneDomicile;
	public String telephonePortable1;
	public String telephonePortable2;
	
	public Adresse adresse;
	
	public Long getCodeAdherent() {
		return codeAdherent;
	}
	
	@Override
    public String toString() {
		return "codeAdherent="+codeAdherent+", nom="+nom+", prenom="+prenom+", emails=["+emails.stream().collect(Collectors.joining(","))+"]"+
				", telephoneBureau="+telephoneBureau+", telephoneDomicile="+telephoneDomicile+", telephonePortable1="+telephonePortable1+", telephonePortable2="+telephonePortable2+", adresse="+adresse.toString();
	}
}
