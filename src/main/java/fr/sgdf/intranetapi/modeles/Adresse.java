package fr.sgdf.intranetapi.modeles;

public class Adresse {
	public String ligne1;
	public String ligne2;
	public String ligne3;
	public String codePostal;
	public String ville;
	public String pays;
	public Boolean npai;
	public String lieuDit;
	
	@Override
    public String toString() {
		return "ligne1="+ligne1+", ligne2="+ligne2+", ligne3="+ligne3+", codePostal="+codePostal+", ville="+ville+", pays="+pays+", npai="+npai;
	}
}
