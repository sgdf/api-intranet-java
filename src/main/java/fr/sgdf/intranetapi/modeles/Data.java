package fr.sgdf.intranetapi.modeles;

import java.util.List;
import java.util.stream.Collectors;

public class Data {
	public List<Error> errors;
	
	@Override
    public String toString() {
		return this.dumpErrors();
	}
	
	public String dumpErrors() {
		return "["+errors.stream().map(Error::toString).collect(Collectors.joining(","))+"]";
	}
}
