package fr.sgdf.intranetapi;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import fr.sgdf.intranetapi.modeles.adherent.actionformation.ActionFormation;
import fr.sgdf.intranetapi.modeles.adherent.actionformation.DataActionFormation;
import fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome;
import fr.sgdf.intranetapi.modeles.adherent.formation.Formation;
import fr.sgdf.intranetapi.modeles.adherent.qualification.Qualification;
import fr.sgdf.intranetapi.modeles.structure.Structure;

/**
 * Classe utilisée pour transformer les structures JSON
 * 
 * @author Scouts et Guides de France
 *
 */
public class ApiParser {
	
	static private ObjectMapper objectMapper_;
	static {
		objectMapper_ = new ObjectMapper();
		objectMapper_.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	/**
	 * Transforme des données structure ({@link Structure}) en JSON 
	 * @param data : L'objet structure ({@link Structure})
	 * @return La version JSON de la structure
	 * @throws ApiException toute erreur
	 */
	public static String encodeStructure(Structure data) throws ApiException {
		try {
			String jsonData = objectMapper_.writeValueAsString(data);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur encodeStructure: ",e);
		}
	}

	/**
	 * Transforme des données JSON en objet structure ({@link Structure})
	 * @param jsonData : données JSON représentant une structure 
	 * @return l'objet structure
	 * @throws ApiException toute erreur
	 */
	public static Structure parseStructure(String jsonData) throws ApiException {
		try {
			Structure data = objectMapper_.readValue(jsonData, Structure.class);
			return data;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseStructure: ",e);
		}
	}
	
	/**
	 * Transforme des données de liste de structure ({@link Structure}) en JSON 
	 * @param data : La Liste d'objet structure ({@link Structure})
	 * @return La version JSON de la structure
	 * @throws ApiException toute erreur
	 */
	public static String encodeStructures(List<Structure> data) throws ApiException {
		try {
			String jsonData = objectMapper_.writeValueAsString(data);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur encodeStructures: ",e);
		}
	}
	
	/**
	 * Transforme des données JSON en une liste de structures ({@link Structure})
	 * @param jsonData : La version JSON de liste de structures
	 * @return La liste des structures ({@link Structure})
	 * @throws ApiException toute erreur
	 */
	public static List<Structure> parseStructures(String jsonData) throws ApiException {
		try {
			Structure[] data = objectMapper_.readValue(jsonData, Structure[].class);
			return Arrays.asList(data);
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseAdherent: ",e);
		}
	}
	
	/**
	 * Transforme des données adherent ({@link Adherent}) en JSON 
	 * @param data : L'objet adherent ({@link Adherent})
	 * @return La version JSON de l'adherent
	 * @throws ApiException toute erreur
	 */
	public static String encodeAdherent(Adherent data) throws ApiException {
		try {
			String jsonData = objectMapper_.writeValueAsString(data);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur encodeAdherent: ",e);
		}
	}

	/**
	 * Transforme des données JSON en objet adherent ({@link Adherent})
	 * @param jsonData : données JSON représentant un adhérent 
	 * @return l'objet adherent
	 * @throws ApiException toute erreur
	 */
	public static Adherent parseAdherent(String jsonData) throws ApiException {
		try {
			Adherent data = objectMapper_.readValue(jsonData, Adherent.class);
			data.compute();
			return data;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseAdherent: ",e);
		}
	}
	
	/**
	 * Transforme des données JSON en une liste d'actions de formation ({@link ActionFormation})
	 * @param jsonData : La version JSON des actions de formation
	 * @return La liste des actions de formation ({@link ActionFormation})
	 * @throws ApiException toute erreur
	 */
	public static List<ActionFormation> parseAdherentActionsFormations(String jsonData) throws ApiException {
		try {
			DataActionFormation data = objectMapper_.readValue(jsonData, DataActionFormation.class);
			return data.data;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseAdherent: ",e);
		}
	}
	
	/**
	 * Transforme des données JSON en une liste de formations ({@link Formation})
	 * @param jsonData : La version JSON des formations
	 * @return La liste des formations ({@link Formation})
	 * @throws ApiException toute erreur
	 */
	public static Formation[] parseFormations(String jsonData) throws ApiException {
		try {
			return objectMapper_.readValue(jsonData, Formation[].class);
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseFormations: ",e);
		}
	}
	
	/**
	 * Transforme la liste de formations d'un adhérent ({@link Formation}) en JSON 
	 * @param data : la liste de formations ({@link Formation})
	 * @return La version JSON des formations
	 * @throws ApiException toute erreur
	 */
	public static String encodeFormations(List<Formation> data) throws ApiException {
		try {
			String jsonData = objectMapper_.writeValueAsString(data);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur encodeFormations: ",e);
		}
	}
	
	/**
	 * Transforme des données JSON en une liste de diplomes ({@link Diplome})
	 * @param jsonData : La version JSON des diplomes
	 * @return La liste des diplomes ({@link Diplome})
	 * @throws ApiException toute erreur
	 */
	public static Diplome[] parseDiplomes(String jsonData) throws ApiException {
		try {
			return objectMapper_.readValue(jsonData, Diplome[].class);
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseDiplomes: ",e);
		}
	}
	
	/**
	 * Transforme la liste de diplomes d'un adhérent ({@link Diplome}) en JSON 
	 * @param data : la liste de formations ({@link Diplome})
	 * @return La version JSON des formations
	 * @throws ApiException toute erreur
	 */
	public static String encodeDiplomes(List<Diplome> data) throws ApiException {
		try {
			String jsonData = objectMapper_.writeValueAsString(data);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur encodeDiplomes: ",e);
		}
	}
	
	/**
	 * Transforme des données JSON en une liste de qualifications ({@link Qualification})
	 * @param jsonData : La version JSON des qualifications
	 * @return La liste des qualifications ({@link Qualification})
	 * @throws ApiException toute erreur
	 */
	public static Qualification[] parseQualifications(String jsonData) throws ApiException {
		try {
			return objectMapper_.readValue(jsonData, Qualification[].class);
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur parseQualifications: ",e);
		}
	}
	
	/**
	 * Transforme la liste de qualifications d'un adhérent ({@link Qualification}) en JSON 
	 * @param data : la liste de formations ({@link Qualification})
	 * @return La version JSON des formations
	 * @throws ApiException toute erreur
	 */
	public static String encodeQualifications(List<Qualification> data) throws ApiException {
		try {
			String jsonData = objectMapper_.writeValueAsString(data);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new ApiException("Erreur encodeQualifications: ",e);
		}
	}
}
