package fr.sgdf.intranetapi.exceptions;

public class ApiTokenException extends Exception {

	private static final long serialVersionUID = 3521210312930078872L;

	public ApiTokenException(String message, Exception e) {
		super(message, e);
	}
	
}
