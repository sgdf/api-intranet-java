package fr.sgdf.intranetapi.exceptions;

public class ApiSessionExhaustedPoolException extends Exception {

	private static final long serialVersionUID = 433516329319410934L;

	public ApiSessionExhaustedPoolException(String message, Exception e) {
		super(message, e);
	}
}
