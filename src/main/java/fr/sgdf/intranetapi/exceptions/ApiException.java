package fr.sgdf.intranetapi.exceptions;

/**
 * Classe d'exception pour tout problème de l'API
 * 
 * @author Scouts et Guides de France
 */
public class ApiException extends Exception {

	private static final long serialVersionUID = 5905255648937991644L;

	public ApiException(String message, Exception e) {
		super(message, e);
	}

	public ApiException(String message) {
		super(message);
	}

}
