package fr.sgdf.intranetapi.exceptions;

import javax.ws.rs.core.Response.StatusType;

import fr.sgdf.intranetapi.TokenError;

/**
 * Classe d'exception pour tout problème d'authentification et session de l'API
 * 
 * @author Scouts et Guides de France
 */
public class ApiSessionException extends Exception {

	private static final long serialVersionUID = -6122630295280306872L;

	public ApiSessionException(String message) {
		super(message);
	}

	public ApiSessionException(String message, Exception e) {
		super(message, e);
	}

	public ApiSessionException(String title, StatusType status, TokenError error) {
		super(title + " : " + dumpStatus(status));
	}
	
	private static String dumpStatus(StatusType status) {
		return status.getStatusCode()+"/"+status.getReasonPhrase();
	}
}
