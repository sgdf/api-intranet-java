package fr.sgdf.intranetapi;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import fr.sgdf.intranetapi.exceptions.ApiSessionException;
import fr.sgdf.intranetapi.utils.JacksonIgnoreResolver;
import io.micrometer.core.instrument.MeterRegistry;

public class ApiCallFactory {
	
	private Client client_;
	private String clientId_;
	private String url_;
	private MeterRegistry registry_;
	
	public ApiCallFactory(final String clientId, String url, boolean addIgnore) throws Exception {
		this.clientId_ = clientId;
		this.url_ = url;
		
		try {
			if (addIgnore) {
		  		JacksonIgnoreResolver resolver = new JacksonIgnoreResolver();
		  		ResteasyProviderFactory.getInstance().registerProviderInstance(resolver);
		  		client_ = new ResteasyClientBuilder().register(resolver).build();
			}
			else {
				client_ = new ResteasyClientBuilder().build();
			}
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
		}
	}
	
	public ApiCallFactory(final String clientId, String url) throws Exception {
		this.clientId_ = clientId;
		this.url_ = url;
		
		try {
		  		JacksonIgnoreResolver resolver = new JacksonIgnoreResolver();
		  		ResteasyProviderFactory.getInstance().registerProviderInstance(resolver);
		  		
		  		client_ = ClientBuilder.newClient();
		  		
		  		client_ = new ResteasyClientBuilder().register(resolver).build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
		}
	}
	
	public void setRegistry(MeterRegistry registry) {
		registry_ = registry;
	}
	
	public Client getClient() {
		return client_;
	}
	
	public ApiCall allocate(String tokenType, String token) throws ApiSessionException {
		ApiCall session = new ApiCall(client_, clientId_, url_, registry_);
		session.updateToken(tokenType, token);
		return session;
	}
}
