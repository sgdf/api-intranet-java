package fr.sgdf.intranetapi.calls;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import fr.sgdf.intranetapi.ApiCall;
import fr.sgdf.intranetapi.ApiCallFactory;
import fr.sgdf.intranetapi.ApiTokenRequester;
import fr.sgdf.intranetapi.Global;
import fr.sgdf.intranetapi.modeles.configuration.Civilite;
import fr.sgdf.intranetapi.modeles.token.Token;

public class TestCivilites {
	
	static private final org.slf4j.Logger logger = LoggerFactory.getLogger(TestCivilites.class);
	
	static private ApiCall call;
	
	@BeforeAll()
	static void prepare() throws Exception {
		Global.prepare();
		
		ApiTokenRequester tr = new ApiTokenRequester(Global.URL, Global.REALMS, Global.CLIENT, Global.CLIENT_SECRET);
		Token t = tr.getToken(null);
		assertNotNull(t);
		
		ApiCallFactory p = new ApiCallFactory(Global.CLIENT_ID, "https://test-api-sgdf-qual.intrassoc.com");
		assertNotNull(p);
		call = p.allocate(t.token_type, t.access_token);
		assertNotNull(call);
	}
	
	@Test()
	public void testCivilites() throws Exception {
		Map<Integer, Civilite> civilites = call.civilites();
		assertTrue(civilites.size() > 0);
		logger.info(civilites.size()+" civilites");
		assertTrue(civilites.get(1).genre == Civilite.Genre.Homme);
		assertTrue(civilites.get(2).genre == Civilite.Genre.Femme);
	}

}
