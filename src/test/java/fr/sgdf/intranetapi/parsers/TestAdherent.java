package fr.sgdf.intranetapi.parsers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.sgdf.intranetapi.ApiParser;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;

public class TestAdherent {
	static private String j = "{\"idInscription\":4299256,\"idStructureFonctionPrincipale\":1119974,\"idCivilite\":1,\"saison\":\"2021-2022\",\"nomNaissance\":\"NOM\",\"dateDeNaissance\":\"1972-05-26T00:00:00\",\"statut\":\"Adhérent(e)\",\"fonctionPrincipale\":{\"inscriptionId\":0,\"structure\":{\"idInvariantStructure\":15883,\"id\":0,\"code\":\"988800100\",\"nom\":\"EQUIPE NATIONALE FORMATION\",\"echelon\":\"National\"},\"fonction\":{\"categorie\":1,\"code\":\"914\",\"nom\":\"CHARGE MISSION \"}},\"fonctionsSecondaires\":[],\"lieuDeNaissance\":{\"codePostal\":\"74000\",\"ville\":\"ANNECY\",\"codeInsee\":\"74010\",\"idPays\":null},\"intervenantJS\":{\"fonctionJS\":\"Adjoint\",\"diplomeJS\":\"Scout Dir\",\"diplomeDetailJS\":\"CA Dir SF\",\"qualiteJS\":\"Dir titulaire-Stagiaire BAFD\",\"derniereModification\":\"2021-06-08T20:32:28\"},\"droitImage\":true,\"droitsInformations\":true,\"codeAdherent\":\"762368487\",\"nom\":\"NOM\",\"prenom\":\"PRENOM\",\"emails\":[\"email@domaine.com\"],\"telephoneBureau\":\"0123456789\",\"telephoneDomicile\":\"0123456789\",\"telephonePortable1\":\"0123456789\",\"telephonePortable2\":\"\",\"adresse\":{\"ligne1\":\"\",\"ligne2\":\"\",\"ligne3\":\"\",\"codePostal\":\"\",\"ville\":\"\",\"pays\":\"FRANCE\",\"npai\":false}}";
	
	@Test()
	public void testParseJSON() throws ApiException {
		Adherent ad = ApiParser.parseAdherent(j);
		assertEquals(ad.codeAdherent, 762368487L);
		assertEquals(ad.nomNaissance, "NOM");
	}
}
