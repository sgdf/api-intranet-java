package fr.sgdf.intranetapi.parsers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import fr.sgdf.intranetapi.ApiParser;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.modeles.structure.Structure;

public class TestStructureParser {
	static private String j = "{\"idInvariantStructure\":13012,\"idSaison\":2022,\"codeStructure\":306710000,\"nomStructure\":\"TERRITOIRE RHIN NORD\",\"typeStructure\":\"Territoire\",\"echelonStructure\":\"Territorial\",\"branche\":\"ADULTE\",\"departementAdministratif\":\"68\"}";
	static private String jl = "[{\"idInvariantStructure\":13012,\"idSaison\":2022,\"codeStructure\":306710000,\"nomStructure\":\"TERRITOIRE RHIN NORD\",\"typeStructure\":\"Territoire\",\"echelonStructure\":\"Territorial\",\"branche\":\"ADULTE\",\"departementAdministratif\":\"68\"}]";
	
	// idInvariantStructure=13012, idSaison=2022, codeStructure=306710000, nomStructure=TERRITOIRE RHIN NORD, typeStructure=Territoire, echelonStructure=Territorial, branche=ADULTE, departementAdministratif=68,description=null,hemisphere=null,adresseLigne1=null,adresseLigne2=null,adresseLigne3=null,codePostal=null,ville=null,pays=null,telephone=null,fax=null,courriel=null,debutActivite=null,infosLocalisation=null,latitude=null,longitude=null
	static private Structure s = new Structure();
	static private List<Structure> sl = new ArrayList<Structure>();
	static {
		s.branche = Branche.ADULTE;
		s.codeStructure = 306710000;
		s.echelonStructure="Territorial";
		s.idInvariantStructure = 13012L;
		s.idSaison = 2022L;
		s.typeStructure = "Territoire";
		s.nomStructure="TERRITOIRE RHIN NORD";
		s.departementAdministratif="68";
		
		sl.add(s);
	}
	
	@Test()
	public void testUnique() throws ApiException {
		String json = ApiParser.encodeStructure(s);
		Structure s2 = ApiParser.parseStructure(json);
		assertEquals(s.branche, s2.branche);
		assertEquals(s.codeStructure, s2.codeStructure);
		assertEquals(s.echelonStructure, s2.echelonStructure);
		assertEquals(s.idInvariantStructure, s2.idInvariantStructure);
		assertEquals(s.idSaison, s2.idSaison);
		assertEquals(s.typeStructure, s2.typeStructure);
		assertEquals(s.nomStructure, s2.nomStructure);
		assertEquals(s.departementAdministratif, s2.departementAdministratif);
	}
	
	@Test()
	public void testList() throws ApiException {
		String json = ApiParser.encodeStructures(sl);
		List<Structure> sl2 = ApiParser.parseStructures(json);
		Structure s2 = sl2.get(0);	
		assertEquals(s.branche, s2.branche);
		assertEquals(s.codeStructure, s2.codeStructure);
		assertEquals(s.echelonStructure, s2.echelonStructure);
		assertEquals(s.idInvariantStructure, s2.idInvariantStructure);
		assertEquals(s.idSaison, s2.idSaison);
		assertEquals(s.typeStructure, s2.typeStructure);
		assertEquals(s.nomStructure, s2.nomStructure);
		assertEquals(s.departementAdministratif, s2.departementAdministratif);
	}
}
