package fr.sgdf.intranetapi.parsers;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import fr.sgdf.intranetapi.utils.StructureUtils;

public class TestsStructure {
	
	static final int NAT = 988300900;
	static final int TERR = 804100000;
	static final int GROUPE = 804100200;
	static final int UNITE = 804100211;
	
	@Test()
	public void testIsNationale() {
		int code = NAT;
		boolean ret = StructureUtils.isNationale(code);
		assertTrue(ret);
	}
	
	@Test()
	public void testIsNationaleTerritoire() {
		int code = TERR;
		boolean ret = StructureUtils.isNationale(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsNationaleGroupe() {
		int code = GROUPE;
		boolean ret = StructureUtils.isNationale(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsNationaleUnite() {
		int code = UNITE;
		boolean ret = StructureUtils.isNationale(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsTerritoire() {
		int code = NAT;
		boolean ret = StructureUtils.isTerritoire(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsTerritoireTerritoire() {
		int code = TERR;
		boolean ret = StructureUtils.isTerritoire(code);
		assertTrue(ret);
	}
	
	@Test()
	public void testIsTerritoireGroupe() {
		int code = GROUPE;
		boolean ret = StructureUtils.isTerritoire(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsTerritoireUnite() {
		int code = UNITE;
		boolean ret = StructureUtils.isTerritoire(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsGroupe() {
		int code = NAT;
		boolean ret = StructureUtils.isGroupe(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsGroupeTerritoire() {
		int code = TERR;
		boolean ret = StructureUtils.isGroupe(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsGroupeGroupe() {
		int code = GROUPE;
		boolean ret = StructureUtils.isGroupe(code);
		assertTrue(ret);
	}
	
	@Test()
	public void testIsGroupeUnite() {
		int code = UNITE;
		boolean ret = StructureUtils.isGroupe(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsUnite() {
		int code = NAT;
		boolean ret = StructureUtils.isUnite(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsUniteTerritoire() {
		int code = TERR;
		boolean ret = StructureUtils.isUnite(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsUniteGroupe() {
		int code = GROUPE;
		boolean ret = StructureUtils.isUnite(code);
		assertFalse(ret);
	}
	
	@Test()
	public void testIsUniteUnite() {
		int code = UNITE;
		boolean ret = StructureUtils.isUnite(code);
		assertTrue(ret);
	}
}
