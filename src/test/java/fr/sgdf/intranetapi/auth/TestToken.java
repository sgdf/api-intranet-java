package fr.sgdf.intranetapi.auth;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.sgdf.intranetapi.ApiTokenRequester;
import fr.sgdf.intranetapi.Global;
import fr.sgdf.intranetapi.exceptions.ApiTokenException;
import fr.sgdf.intranetapi.modeles.token.Token;

public class TestToken {
	
	@BeforeAll()
	static void prepare() {
		Global.prepare();	
	}
	
	@Test()
	public void testToken() throws ApiTokenException {
		ApiTokenRequester tr = new ApiTokenRequester(Global.URL, Global.REALMS, Global.CLIENT, Global.CLIENT_SECRET);
		Token t = tr.getToken(null);
		assertNotNull(t);
		assertTrue(t.token_type.equals(Global.TOKEN_TYPE));
		assertTrue(t.expires_in > 0);
	}
}
