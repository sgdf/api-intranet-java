package fr.sgdf.intranetapi;

public class Global {
	
	static final public String URL = "https://auth.sgdf-qual.intrassoc.com/auth/";
	static final public String REALMS = "sgdf_qualification";
	
	static final public String TOKEN_TYPE = "Bearer";
	
	static public String CLIENT;
	static public String CLIENT_SECRET;
	static public String CLIENT_ID;
	
	static public void prepare()
	{
		CLIENT = System.getProperty("CLIENT");
		CLIENT_SECRET = System.getProperty("CLIENT_SECRET");
		CLIENT_ID = System.getProperty("CLIENT_ID");
	}
}
